
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from 'app/main/auth/auth.service';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private _auth: AuthService,
        private _router: Router,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
        //const currentUser = this._auth.currentUserValue;
        if (localStorage.getItem('Site') != undefined && localStorage.getItem('Site') != null &&
         parseInt(localStorage.getItem('Site')) == 2) {

            if ((parseInt(localStorage.getItem('UserTypeId')) != undefined &&
             parseInt(localStorage.getItem('UserTypeId')) != null && 
             (parseInt(localStorage.getItem('UserTypeId')) == 1 || (parseInt(localStorage.getItem('UserTypeId'))==2)))&&
             (parseInt(localStorage.getItem('isLoggedin'))==1)) {
                return true;
            }
            else {
               
                this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url } });
                return false;
            }

        }
        else {
            if (localStorage.getItem('Site') != undefined && localStorage.getItem('Site') != null
             && parseInt(localStorage.getItem('Site')) == 1) {
                this.router.navigate(['/error403']);
                return false;
            }
            else {
               
                // not logged in so redirect to login page with the return url
                this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url } });
                return false;
            }
        }
    }
}


@Injectable({ providedIn: 'root' })
export class AuthGuard2 implements CanActivate {
    constructor(
        private router: Router,
        private _auth: AuthService,
        private _router: Router,
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
   
        //const currentUser = this._auth.currentUserValue;
        // if (localStorage.getItem('isLoggedin') == "1" && localStorage.getItem('isLoggedin') != undefined) {
        //     return true;

        // }
        if ((sessionStorage.getItem('isLoggedin') == "1" && sessionStorage.getItem('isLoggedin') != undefined)) {
            return true;

        }
        else if (localStorage.getItem('isLoggedin') == "1" && localStorage.getItem('isLoggedin') != undefined) {
           
            return true;
        }
        else {
     
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
            return false;
          // }
        }
    }
}