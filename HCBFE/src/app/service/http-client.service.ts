import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { environment } from 'environments/environment';

@Injectable()
export class HttpClientsService {
    constructor(private http: HttpClient, private _authentication: AuthenticationService, private zone: NgZone, private router: Router) { }
    //   private baseUrl:environment.baseUrl

    getAPI(url: string): Observable<any> {
        let httpOptions: any;
        httpOptions = { headers: this._authentication.GetHeader() };
        return this.http.get<any>(environment.baseUrl + url, httpOptions);
    }

    postAPI(url: string, data: any): Observable<any> {
        return this.http.post<any>(environment.baseUrl + url, data, {
            headers: this._authentication.GetHeader()
        })
        // .pipe(catchError(this.handleError('get', data)));
    }


    postFile(url: string, data: any): Observable<any> {
        return this.http.post<any>(environment.baseUrl + url, data, {
            reportProgress: true,
            observe: 'events',
            headers: this._authentication.GetFileHeader()
        })
    }

    // deleteAPI(url: string): Observable<any> {
    //     return this.http.delete<any>(this.baseUrl + url, { headers: this.commonService.GetHeader() })
    //         .pipe(catchError(this.handleError('delete')));
    // }

    // private handleError<T>(operation = 'operation', result?: T) {
    //     return (error: any): Observable<T> => {
    //          if (error.status == "401") {
    //             let id = error.status;
    //             this.zone.run(() => {
    //                 this.router.navigate(['/login']);
    //             });
    //         }
    //         //else {
    //         //    this.zone.run(() => {
    //         //        this.router.navigate(['/login']);
    //         //    });
    //         //}
    //         return Observable.of(error.error as T);
    //     };
    // }
}