import { Component, OnInit, ViewChild, Inject, ViewEncapsulation } from '@angular/core';
import { MatSort, MatPaginator, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-open-close-scheme-activity',
  templateUrl: './open-close-scheme-activity.component.html',
  styleUrls: ['./open-close-scheme-activity.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class OpenCloseSchemeActivityComponent implements OnInit {

  errorText: string;
  Working: boolean;
  dataSource: any;
  dialogTitle: string;

 // displayedColumns: string[] = ['HCBReferenceNo', 'ServiceType', 'CaseClosedDate','CaseCreatedOn', 'Id'];
  displayedColumns: string[] = ['HCBReferenceNo', 'CompanyName','CaseCreatedOn','ClaimantName', 'Id'];
  

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _router: Router,
    public dialogRef: MatDialogRef<OpenCloseSchemeActivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
 
    if (this.data.Id == 1) {
      this.dialogTitle = "New Cases"
    } else {
      this.dialogTitle = "New Cases"
    }
    this.dataSource = new MatTableDataSource<any>(this.data.case);

    this.dataSource.paginator = this.paginator;
  }

  Edit(hcbReferenceId: number): void {
    this.dialogRef.close();
    this._router.navigate(["/case-managment/addedit-case-managment/edit", hcbReferenceId]);
    
}
}
