import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../Material.module';
import { AuthGuard2 } from 'app/custom/auth.guard';
import { GoogleChartsModule } from 'angular-google-charts';
import { OpenCloseSchemeActivityComponent } from './open-close-scheme-activity/open-close-scheme-activity.component';
import { ListOfTotalValuesComponent } from './list-of-total-values/list-of-total-values.component';

const routes = [
  {
      path     : 'dashboard',
      component: DashboardComponent,
      canActivate: [AuthGuard2]
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
    OpenCloseSchemeActivityComponent,
    ListOfTotalValuesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    FuseSharedModule,
    MaterialModule,
    DateRangePickerModule,
    FuseWidgetModule,
    NgxChartsModule,
    GoogleChartsModule
  ],
  entryComponents: [
  OpenCloseSchemeActivityComponent,
 ListOfTotalValuesComponent

   ]
})
export class DashboardModule { }
