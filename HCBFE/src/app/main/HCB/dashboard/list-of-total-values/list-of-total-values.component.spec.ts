import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfTotalValuesComponent } from './list-of-total-values.component';

describe('ListOfTotalValuesComponent', () => {
  let component: ListOfTotalValuesComponent;
  let fixture: ComponentFixture<ListOfTotalValuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfTotalValuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfTotalValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
