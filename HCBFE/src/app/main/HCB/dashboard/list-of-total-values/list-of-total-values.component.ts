import { Component, OnInit, ViewEncapsulation, ViewChild, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatSort, MatPaginator, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
@Component({
  selector: 'app-list-of-total-values',
  templateUrl: './list-of-total-values.component.html',
  styleUrls: ['./list-of-total-values.component.scss']
})
export class ListOfTotalValuesComponent implements OnInit {
  errorText: string;
  Working: boolean;
  dataSource: any;
  dialogTitle: string;
  columndisplay: boolean;

  displayedColumns: string[];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _router: Router,
    public dialogRef: MatDialogRef<ListOfTotalValuesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {

    if (this.data.Id == 0) {
     this. displayedColumns= ['HCBReferenceNo', 'CompanyName'];
      this.dialogTitle = "Open Case List"
    }
    if (this.data.Id == 1) {
      this. displayedColumns= ['HCBReferenceNo', 'CompanyName'];
      this.dialogTitle = "Closed Case List"
    }
    if (this.data.Id == 2) {
      this. displayedColumns = ['HCBReferenceNo', 'CompanyName', 'EmployerName']
      this.dialogTitle = "Employers List"
    }
    if (this.data.Id == 3) {
      this. displayedColumns= ['HCBReferenceNo', 'CompanyName', 'Duration']
      this.dialogTitle = "RTW List"
    }
    if (this.data.Id == 4) {
      this. displayedColumns= ['HCBReferenceNo', 'CompanyName'];
      this.dialogTitle = "Active Client Case List"
    }
    this.data.case;
    this.dataSource = new MatTableDataSource<any>(this.data.case);

    this.dataSource.paginator = this.paginator;
  }


}
