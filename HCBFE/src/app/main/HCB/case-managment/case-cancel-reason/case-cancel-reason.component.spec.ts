import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseCancelReasonComponent } from './case-cancel-reason.component';

describe('CaseCancelReasonComponent', () => {
  let component: CaseCancelReasonComponent;
  let fixture: ComponentFixture<CaseCancelReasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseCancelReasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseCancelReasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
