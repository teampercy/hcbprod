import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaseManagmentComponent } from './case-managment.component';
import { AuthGuard2 } from 'app/custom/auth.guard';
import { AddeditCaseManagmentComponent } from './addedit-case-managment/addedit-case-managment.component';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/main/Material.module';
import { AddeditTelephoneActivityComponent } from './addedit-telephone-activity/addedit-telephone-activity.component';
import { AddeditVisitActivityComponent } from './addedit-visit-activity/addedit-visit-activity.component';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import * as _moment from 'moment';
import { AddeditSubsequentActivityComponent } from './addedit-case-managment/addedit-subsequent-activity/addedit-subsequent-activity.component';
import { AddeditAnnualReportSummaryComponent } from './addedit-case-managment/addedit-annual-report-summary/addedit-annual-report-summary.component';
import { AddRemoveFailedCallComponent } from './addedit-telephone-activity/add-remove-failed-call/add-remove-failed-call.component';
import { CaseCancelReasonComponent } from './case-cancel-reason/case-cancel-reason.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { NgxDocumentViewerComponent } from './ngx-document-viewer/ngx-document-viewer.component';
import { AlertMessageComponent } from 'app/main/Common/alert-message/alert-message.component';
import { CaseActivitiesComponent } from './case-activities/case-activities.component';
import { HCBCaseNoteComponent } from './addedit-case-managment/hcbcase-note/hcbcase-note.component';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import {SafeHtmlPipe} from '@fuse/pipes/SafeHtml.pipe';

const moment = _moment;
const routes = [
  {
    path: '',
    component: CaseManagmentComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'addedit-case-managment',
    component: AddeditCaseManagmentComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'addedit-case-managment/edit/:id',
    component: AddeditCaseManagmentComponent,
    canActivate: [AuthGuard2]
  },
  {
    path: 'addedit-case-managment/Reopen/:id',
    component: AddeditCaseManagmentComponent,
    canActivate: [AuthGuard2]
  },

  {
    path: 'ngx-document-viewer/:docName',
    component: NgxDocumentViewerComponent,
    canActivate: [AuthGuard2]
  },
];
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@NgModule({
  declarations: [
    CaseManagmentComponent,
    AddeditCaseManagmentComponent,
    AddeditTelephoneActivityComponent,
    AddeditVisitActivityComponent,
    AddeditSubsequentActivityComponent,
    AddeditAnnualReportSummaryComponent,
    AddRemoveFailedCallComponent,
    CaseCancelReasonComponent,
    NgxDocumentViewerComponent,
    AlertMessageComponent,
    CaseActivitiesComponent,
    CaseActivitiesComponent,
    HCBCaseNoteComponent,
    SafeHtmlPipe
    

    //AddeditAssessorComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FuseSharedModule,
    MaterialModule,
    NgxDocViewerModule,
    RichTextEditorAllModule
  ],
  providers: [

    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    //{ provide: MAT_DATE_FORMATS, useValue: MY_FORMATS1 },
  ],
  entryComponents: [
    AddeditTelephoneActivityComponent,
    AddeditVisitActivityComponent,
    AddeditSubsequentActivityComponent,
    AddeditAnnualReportSummaryComponent,
    AddRemoveFailedCallComponent,
    CaseCancelReasonComponent,
   AlertMessageComponent,
   CaseActivitiesComponent
  //  AddeditAssessorComponent
  ]
})

export class CaseManagmentModule { }
