import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditVisitActivityComponent } from './addedit-visit-activity.component';

describe('AddeditVisitActivityComponent', () => {
  let component: AddeditVisitActivityComponent;
  let fixture: ComponentFixture<AddeditVisitActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditVisitActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditVisitActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
