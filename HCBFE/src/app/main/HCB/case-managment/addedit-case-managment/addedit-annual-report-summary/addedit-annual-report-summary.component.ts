import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CaseManagmentService } from '../../case-managment.service';
import { CommonService } from 'app/service/common.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { GetAnnualReportSummary } from '../addedit-case-management.model';
import * as _moment from 'moment';
import { AppCustomDirective } from 'app/main/Common/appValidation';
import { fuseAnimations } from '@fuse/animations';
const moment = _moment;

@Component({
  selector: 'app-addedit-annual-report-summary',
  templateUrl: './addedit-annual-report-summary.component.html',
  styleUrls: ['./addedit-annual-report-summary.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class AddeditAnnualReportSummaryComponent implements OnInit {

  AnnualReportSummary: FormGroup;
  errorText: string;
  working: boolean;
  dialog: any;
  ChangeColor:string;
  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _caseManagment: CaseManagmentService,
    private _commonService:CommonService,
    private _matSnackBar:MatSnackBar,
    public dialogRef: MatDialogRef<AddeditAnnualReportSummaryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  
  ngOnInit() {
    if (this.data.AnnualReportId) {
      this.AnnualReportSummary = this._formbuilder.group({
        AnnualReportId: [this.data.AnnualReportId],
        HCBReferenceId: [this.data.HCBReferenceId],
        AnnualReportDate: ['',AppCustomDirective.fromDateValidator],
        AnnualReportOutcome: [''],
        AnnualReportValue: ['']
      });
      this.SetAnnualReportSummary();
    }
    else {
      this.AnnualReportSummary = this._formbuilder.group({
        HCBReferenceId: [this.data.HCBReferenceId],
        AnnualReportDate: ['',AppCustomDirective.fromDateValidator],
        AnnualReportOutcome: [''],
        AnnualReportValue: ['']
      });
    }
  }
  onAnnualReportOutComeChange() {
    if (this.AnnualReportSummary.get('AnnualReportOutcome').value == 'Deficit') {
      this.ChangeColor = 'warn';
    } else {
      this.ChangeColor = 'default';
    }
  }

  SetAnnualReportSummary(): void {
  
    this._caseManagment.GetAnnualReportSummaryById(this.data.AnnualReportId)
      .subscribe((response: GetAnnualReportSummary) => {
     
        if (response['result'][0].annualReportDate != "" && response['result'][0].annualReportDate != null)
        this.AnnualReportSummary.patchValue({ AnnualReportDate: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].annualReportDate), month: this._commonService.GetMonthFromDateString(response['result'][0].annualReportDate), date: this._commonService.GetDateFromDateString(response['result'][0].annualReportDate) }) });
       // this.AnnualReportSummary.patchValue({ AnnualReportDate: response['result'][0].annualReportDate});
        this.AnnualReportSummary.patchValue({ AnnualReportOutcome: response['result'][0].annualReportOutcome });
        this.AnnualReportSummary.patchValue({ AnnualReportValue: response['result'][0].annualReportValue });
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      })
  }

  SaveAnnualReportSummary(): void {
    const updatAnnualReportSummary: GetAnnualReportSummary = Object.assign({}, this.AnnualReportSummary.value);
    updatAnnualReportSummary.AnnualReportDate = (this.AnnualReportSummary.value.AnnualReportDate == "" || this.AnnualReportSummary.value.AnnualReportDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.AnnualReportSummary.value.AnnualReportDate) + "-" + (  this._commonService.GetMonthFromDateString(this.AnnualReportSummary.value.AnnualReportDate) + 1) + "-" +  this._commonService.GetDateFromDateString(this.AnnualReportSummary.value.AnnualReportDate) + " 00:00:00:000";
    updatAnnualReportSummary.HCBReferenceId = this.data.HCBReferenceId;
    if (this.data.AnnualReportId) {
      this.working = true;
      this._caseManagment.UpdateAnnualReportSummary(updatAnnualReportSummary)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
          this.working = false;
        });

    } else {
      this.working = true;
      this._caseManagment.SaveAnnualReportSummary(updatAnnualReportSummary)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
        });
    }
  }
  errorMessage(errortext) {
    this._matSnackBar.open(errortext, 'x', {
      verticalPosition: 'top',
      duration: 5000,
      panelClass: ['snackbarError']
    });
  }
}
