import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CaseManagmentService } from '../case-managment.service';
//import { GetVisitActivityById, SaveUpdateHCBVisitActivity } from './visit-activity.model';
import{SaveUpdateCaseActivity} from './Case-activities.model';
import { GetddlTypelistValues } from '../addedit-case-managment/addedit-case-management.model';
import { CommonService } from 'app/service/common.service';
import * as _moment from 'moment';
import { MasterService } from 'app/main/CRM/administration/master/master.service';
import { AppCustomDirective } from 'app/main/Common/appValidation';
const moment = _moment;


@Component({
  selector: 'app-case-activities',
  templateUrl: './case-activities.component.html',
  styleUrls: ['./case-activities.component.scss']
})
export class CaseActivitiesComponent implements OnInit {
  CaseActivity: FormGroup;
  getddlClaimAssessor: GetddlTypelistValues[];
  getddlAction: GetddlTypelistValues[];
  getddlServiceProvider:GetddlTypelistValues[];
  errorText: string;
  working: boolean;
  dialog: any;

  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _caseManagment: CaseManagmentService,
    private _masterService: MasterService,
    private _commonService:CommonService,
    private _matSnackBar: MatSnackBar,
    public dialogRef: MatDialogRef<CaseActivitiesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  
  ngOnInit() {
    this._masterService.GetddlValues(38)
    .subscribe(
      (response: GetddlTypelistValues[]) => this.getddlClaimAssessor = response['result'],
      (error: any) => {
        this.errorText = error;
      });

  this._masterService.GetddlValues(39)
    .subscribe(
      (response: GetddlTypelistValues[]) => this.getddlAction = response['result'],
      (error: any) => {
        this.errorText = error;
      });

// this._masterService.GetddlValues(41)
// .subscribe(
//   (response: GetddlTypelistValues[]) => this.getddlServiceProvider = response['result'],
//   (error: any) => {
//     this.errorText = error;
//   });

this._masterService.GetServiceProviderCompany()
.subscribe((response: any) => 
  this.getddlServiceProvider = response['result'],
(error: any) => {
  this.errorText = error;
  this.working = false;
});

    if (this.data.CaseActivityId) {
      this.CaseActivity = this._formbuilder.group({
       CaseActionId: [this.data.CaseActivityId],
        HCBReferenceId: [this.data.HCBReferenceId],
        DateOfActivity: ['', AppCustomDirective.fromDateValidator],
        ClaimAssessorId: [''],
        ActionId: [''],
        ServiceProviderId: [''],
        ReportDueBy: ['', AppCustomDirective.fromDateValidator]
      });
      this.SetCaseActivity();
    }
    else {
      this.CaseActivity = this._formbuilder.group({
        HCBReferenceId: [this.data.HCBReferenceId],
        DateOfActivity: ['', AppCustomDirective.fromDateValidator],
        ClaimAssessorId: [''],
        ActionId: [''],
        ServiceProviderId: [''],
        ReportDueBy: ['', AppCustomDirective.fromDateValidator]
      });
    }
  }

  SetCaseActivity(): void {
  
    this._caseManagment.GetCaseActivityDetails(this.data.CaseActivityId)
      .subscribe((response: any) => {
        
      //  this.VisitActivity.patchValue({ AppointmentDate: response['result'][0].appointmentDate });
        if (response['result'][0].dateOfActivity != "" && response['result'][0].dateOfActivity != null)
        this.CaseActivity.patchValue({ DateOfActivity: moment({ year: this._commonService.GetYearFromDateString(response['result'][0].dateOfActivity), month: this._commonService.GetMonthFromDateString(response['result'][0].dateOfActivity), date: this._commonService.GetDateFromDateString(response['result'][0].dateOfActivity) }) });
       // this.VisitActivity.patchValue({ ReportCompDate: response['result'][0].reportCompDate });
      
        this.CaseActivity.patchValue({ ClaimAssessorId: response['result'][0].claimAssessorId });
        this.CaseActivity.patchValue({ ActionId: response['result'][0].actionId });
        this.CaseActivity.patchValue({ ServiceProviderId: response['result'][0].serviceProviderId });
       
        this.CaseActivity.patchValue({ ReportDueBy: response['result'][0].reportDueBy});

        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      })
  }

  SaveCaseActivity(): void {

    const updateHCBCaseActivity: SaveUpdateCaseActivity = Object.assign({}, this.CaseActivity.value);
   // updateHCBCaseActivity.DateOfActivity = (this.CaseActivity.value.DateOfActivity == "" || this.CaseActivity.value.DateOfActivity == null) ? "" : "" + this.CaseActivity.value.DateOfActivity._i.year + "-" + (this.CaseActivity.value.DateOfActivity._i.month + 1) + "-" + this.CaseActivity.value.DateOfActivity._i.date + " 00:00:00:000";
   
    // updateHCBCaseActivity.ReportDueBy = (this.CaseActivity.value.ReportDueBy == "" || this.CaseActivity.value.ReportDueBy == null) ? "" : "" + this.CaseActivity.value.ReportDueBy._i.year + "-" + (this.CaseActivity.value.ReportDueBy._i.month + 1) + "-" + this.CaseActivity.value.ReportDueBy._i.date + " 00:00:00:000";
    updateHCBCaseActivity.DateOfActivity = (this.CaseActivity.value.DateOfActivity == "" || this.CaseActivity.value.DateOfActivity == null) ? null : "" + this._commonService.GetYearFromDateString(this.CaseActivity.value.DateOfActivity) + "-" + (this._commonService.GetMonthFromDateString(this.CaseActivity.value.DateOfActivity) + 1) + "-" + this._commonService.GetDateFromDateString(this.CaseActivity.value.DateOfActivity) + " 00:00:00:000";
   
    updateHCBCaseActivity.ReportDueBy = (this.CaseActivity.value.ReportDueBy== "" || this.CaseActivity.value.ReportDueBy== null) ? null : "" + this._commonService.GetYearFromDateString(this.CaseActivity.value.ReportDueBy) + "-" + (this._commonService.GetMonthFromDateString(this.CaseActivity.value.ReportDueBy) + 1) + "-" + this._commonService.GetDateFromDateString(this.CaseActivity.value.ReportDueBy) + " 00:00:00:000";

   
    updateHCBCaseActivity.HCBReferenceId = this.data.HCBReferenceId;
    if (this.data.CaseActivityId) {
 
      this.working = true;
      updateHCBCaseActivity.CaseActivityId = this.data.CaseActivityId;
      this._caseManagment.UpdateCasesActivity(updateHCBCaseActivity)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
          this.working = false;
        });

    } else {

      this.working = true;
     
      this._caseManagment.SaveCaseActivity(updateHCBCaseActivity)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
        });
    }
  }
  errorMessage(errortext) {
    this._matSnackBar.open(errortext, 'x', {
      verticalPosition: 'top',
      duration: 5000,
      panelClass: ['snackbarError']
    });
  }

}
