import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CaseManagmentService } from '../../case-managment.service';
import { SaveUpdateFailedCall } from '../telephone-activity.model';
import { CommonService } from 'app/service/common.service';

@Component({
  selector: 'app-add-remove-failed-call',
  templateUrl: './add-remove-failed-call.component.html',
  styleUrls: ['./add-remove-failed-call.component.scss'],
  animations: [fuseAnimations],
  encapsulation: ViewEncapsulation.None
})
export class AddRemoveFailedCallComponent implements OnInit {

  FailedCall: FormGroup;
  errorText: string;
  working: boolean;
  dialog: any;

  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _route: ActivatedRoute,
    private _caseManagmentService: CaseManagmentService,
    private _commonService: CommonService,
    private _matSnackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AddRemoveFailedCallComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {

    if (this.data.FailedCallId) {
      this.FailedCall = this._formbuilder.group({
        FailedCallId: [this.data.FailedCallId],
        HCBTelephoneID: [this.data.HCBTelephoneID],
        HCBReferenceId: [this.data.HCBReferenceId],
        failCallDate: [''],
        failCallTime: ['']
      });
      // this.SetReferralContact();
    }
    else if (this.data.FailedCallId == undefined) {
      this.FailedCall = this._formbuilder.group({
        HCBTelephoneID: [this.data.HCBTelephoneID],
        HCBReferenceId: [this.data.HCBReferenceId],
        failCallDate: [''],
        failCallTime: ['']
      });
      //  this.SetValueForUndefined();
    }
    else {
      this.FailedCall = this._formbuilder.group({
        HCBTelephoneID: [this.data.HCBTelephoneID],
        HCBReferenceId: [this.data.HCBReferenceId],
        failCallDate: [''],
        failCallTime: ['']
      });

    }

  }


  // SetFailedCall():void {

  //   this._caseManagmentService.GetReferralContact(this.data.HCBTelephoneId)
  //     .subscribe((response: GetReferralContact) => {

  //       this.FailedCall.patchValue({ contactName: response['result'][0].contactName });

  //       this.FailedCall.patchValue({ jobTitle: response['result'][0].jobTitle });
  //       this.FailedCall.patchValue({ phoneNo: response['result'][0].phoneNo });
  //       this.FailedCall.patchValue({ nonHOLocation: response['result'][0].nonHOLocation });
  //       this.FailedCall.patchValue({ contactEmail:response['result'][0].contactEmail });
  //       this.working = false;
  //     }, (error: any) => {
  //       this.errorText = error;
  //       this.working = false;
  //     })
  // }

  Save(): void {

    var today = new Date();
    console.log(today);
  
    this.working = true;
    const updateFailedCall: SaveUpdateFailedCall = Object.assign({}, this.FailedCall.value);
 
    if(this.data.HCBTelephoneID ==0)
    {
      updateFailedCall.Id= this.data.FailedCallList.length;
      updateFailedCall.failCallDate = (this.FailedCall.value.failCallDate == "" || this.FailedCall.value.failCallDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.FailedCall.value.failCallDate) + "-" + (this._commonService.GetMonthFromDateString(this.FailedCall.value.failCallDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.FailedCall.value.failCallDate) + " 00:00:00:000";
      updateFailedCall.failCallTime = (this.FailedCall.value.failCallTime == "" || this.FailedCall.value.failCallTime == null) ? null : "" +"1900-01-01T"+this.FailedCall.value.failCallTime + ":00";
     
     // updateFailedCall.failCallTime = (this.FailedCall.value.failCallTime == "" || this.FailedCall.value.failCallTime == null) ? null : "" + this.FailedCall.value.failCallTime + ":00";
     //updateFailedCall.failCallTime = (this.FailedCall.value.failCallTime == "" || this.FailedCall.value.failCallTime == null) ? null : "" +"1900-01-01T"+ this.FailedCall.value.failCallTime + ":00";
     this.data.FailedCallList.push(updateFailedCall);
    }
    if (this.data.HCBTelephoneID > 0) {
      updateFailedCall.failCallDate = (this.FailedCall.value.failCallDate == "" || this.FailedCall.value.failCallDate == null) ? null : "" + this._commonService.GetYearFromDateString(this.FailedCall.value.failCallDate) + "-" + (this._commonService.GetMonthFromDateString(this.FailedCall.value.failCallDate) + 1) + "-" + this._commonService.GetDateFromDateString(this.FailedCall.value.failCallDate) + " 00:00:00:000";
      updateFailedCall.failCallTime = (this.FailedCall.value.failCallTime == "" || this.FailedCall.value.failCallTime == null) ? null : "" + this.FailedCall.value.failCallTime + ":00";
     
      this._caseManagmentService.SaveFailedCall(updateFailedCall)
        .subscribe((response: number) => {

        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
          this.working = false;
        });
    }
    this.dialogRef.close(true);
    this.working = false;
  }
  errorMessage(errortext) {
    this._matSnackBar.open(errortext, 'x', {
      verticalPosition: 'top',
      duration: 5000,
      panelClass: ['snackbarError']
    });
  }
}
