import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddeditTelephoneActivityComponent } from './addedit-telephone-activity.component';

describe('AddeditTelephoneActivityComponent', () => {
  let component: AddeditTelephoneActivityComponent;
  let fixture: ComponentFixture<AddeditTelephoneActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddeditTelephoneActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddeditTelephoneActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
