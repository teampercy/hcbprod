export class HCBClientGrid {
    nurseName: string;
    claimHandlerID: number;
    claimHandlerName: string;
    clientName: string;
    clientDOB: Date;
    refNo: string;
    hcbReceivedDate: Date;
    dateClosed: Date;
    clientContact: string;
}

export class GetHCBClientGrid {
    pageIndex: number;
    pageSize: number;
    sort: string;
    direction: string;
    HCBReferenceNo: string;
    ClientId: number;
    ClaimantDOB: string;
   // ClientContact: string;
    AssessorId: number;
    CaseMgrId: number;
    ClaimantFirstName:string;
    ClaimantLastName: string;
    HCBReceivedDate:string;
    usertype: string;
    UserId: number;
    Isopen: number;

}

export class HCBClientLength {
    Length: number;
}

export class HCBClient {
    hcbClientGrid: HCBClientGrid[];
    hcbClientLength: HCBClientLength[];
}