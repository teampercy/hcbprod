import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatPaginator, MatDialog, MatTableDataSource, MatSort, PageEvent, Sort } from '@angular/material';
import { Router } from '@angular/router';
import { CaseManagmentService } from './case-managment.service';
import { GetHCBClientGrid, HCBClientGrid } from './case-managment.model';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { GetddlClientModel, GetddlCasemanagmentModel,GetAssessorDetails } from './addedit-case-managment/addedit-case-management.model';
import { MasterService } from 'app/main/CRM/administration/master/master.service';


@Component({
  selector: 'app-case-managment',
  templateUrl: './case-managment.component.html',
  styleUrls: ['./case-managment.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class CaseManagmentComponent implements OnInit {

  Reopen:boolean;
 
  errorText: string;
  Message:string;
  working: boolean;
  CaseTitle: string="Open Cases";
  filterDrp: FormGroup;
  caseData: any;
  hcbClientRecord: any;
  pageIndex: number;
  pageSize: number;
  sortColumn: string;
  sortDirection: 'asc' | 'desc' | '';
  getHCBClientGrid: GetHCBClientGrid;
  getddlCaseOwner: GetddlCasemanagmentModel[];
  getddlAssessorDetails: GetAssessorDetails[];
  UserId: number;
  usertype: string;
  userTypeId: number;
  Isopen: number = 1;  //initially it was 2 for all cases 21_07_2020 now for open cases
  TotalRecords: number;
  AddCaseDisabled: boolean;

  ForCaseManager:boolean;

  getddlClient: GetddlClientModel[];
  //displayedColumns: string[] = ['HCBReferenceNo', 'ClientId', 'HCBReceivedDate', 'ServiceType', 'CaseMgrId','ClaimantFirstName','ClaimantLastName', 'Action'];
  displayedColumns: string[] = ['HCBReferenceNo', 'ClientId', 'HCBReceivedDate', 'ServiceType', 'CaseMgrId','ClaimantName', 'Action'];


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    public dialog: MatDialog,
    private _caseManagment: CaseManagmentService,
    private _masterService: MasterService
  ) { }

  ngOnInit() {

    this._caseManagment.GetddlClient()
      .subscribe((response: GetddlClientModel[]) => this.getddlClient = response['result'],
        (error: any) => {
          this.errorText = error;
        });

    this._caseManagment.GetddlCaseManager()
      .subscribe(
        (response: GetddlCasemanagmentModel[]) => this.getddlCaseOwner = response['result'],
        (error: any) => {
          this.errorText = error;
        });

        this._masterService.GetAssessorGrid()
      .subscribe((response: any) => {
        this.getddlAssessorDetails = response['result'];
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      });

    this.usertype = localStorage.getItem('UserType');
    this.UserId = parseInt(localStorage.getItem('UserId'));
    if (parseInt(localStorage.getItem('UserTypeId')) == 3||parseInt(localStorage.getItem('UserTypeId')) == 4) {
      this.AddCaseDisabled = false;
    }
    else {
      this.AddCaseDisabled = true;
    }

    this.pageIndex = 0;
    this.pageSize = 15;
    this.sortColumn = 'HCBReferenceId';
    this.sortDirection = 'asc';

    this.getHCBClientGrid = {
      pageIndex: this.pageIndex,
      pageSize: this.pageSize,
      sort: this.sortColumn,
      direction: this.sortDirection,
      HCBReferenceNo: null,
      ClientId: null,
      ClaimantDOB: null,
     // ClientContact: null,
      AssessorId: null,
      CaseMgrId: null,
      ClaimantFirstName:null,
      ClaimantLastName:null,
      HCBReceivedDate:null,
      usertype: this.usertype,
      UserId: this.UserId,
      Isopen: this.Isopen
    }

    this.GetHCBClientGrid(this.getHCBClientGrid);

    this.filterDrp = this._formBuilder.group({
      refNo: [null],
      clientName: [null],
      Title: [null],
      City: [null],
      State: [null],
      ZipCode: [null],
      clientDOB: [null],
      hcbReceivedDate:[null],
     // clientContact: [null],
     AssessorId: [null],
      CaseMgrId: [null],
      ClaimantFirstName:[null],
      ClaimantLastName:[null]
    });
    //this.dataSource.paginator = this.paginator;

    if(this.Isopen==1)
    {
      this.Reopen==false;
    }
    else{
      this.Reopen==true;
    }
  }

  onCaseChange(event: any): void {
    this.pageIndex=0;
    this.getHCBClientGrid.pageIndex=0;
    if (parseInt(event.source.value) == 1) {
      this.CaseTitle ="Open Cases"
      this.Reopen=false;
    }
    else if(parseInt(event.source.value) == 0)
    {
      this.CaseTitle="Closed Cases"
      this.Reopen=true;
    }
    else {
      this.CaseTitle="All"
    }
    this.getHCBClientGrid.Isopen = parseInt(event.source.value);

    this.GetHCBClientGrid(this.getHCBClientGrid);
  }

  GetHCBClientGrid(model: GetHCBClientGrid): void {
  
    this.working = true;
    this.hcbClientRecord = [];
    this.paginator = null;
    this._caseManagment.GetHCBClientGrid(model)
      .subscribe((response: HCBClientGrid) => {
    debugger;
        if(response['result'].hcbClientGrid.length>0)
        {
          if(parseInt(localStorage.getItem('UserTypeId')) == 3){
           this.ForCaseManager=true;
          }else{
            this.ForCaseManager=false;
          }
        this.hcbClientRecord = new MatTableDataSource<HCBClientGrid>(response['result'].hcbClientGrid);
        this.TotalRecords = response['result'].hcbClientLength[0].totalLength;

        this.hcbClientRecord.sort = this.sort;
        this.hcbClientRecord.paginator = this.paginator;
        this.errorText=" ";
        }else{
          this.errorText="No Case Record available";
        }
        this.working = false;

      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddCaseRecord() {
    this._router.navigate(['/case-managment/addedit-case-managment']);
  }

  EditCaseRecord(hcbReferenceId: number): void {
    this._router.navigate(["/case-managment/addedit-case-managment/edit", hcbReferenceId]);
  }

  ReponeCase(hcbReferenceId: number): void {
    this._router.navigate(["/case-managment/addedit-case-managment/Reopen", hcbReferenceId]);
  }

  pageChanged(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getHCBClientGrid.pageIndex = event.pageIndex;
    this.getHCBClientGrid.pageSize = event.pageSize;
    this.GetHCBClientGrid(this.getHCBClientGrid);
  }

  sortChange(event: Sort): void {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getHCBClientGrid.sort = event.active;
    this.getHCBClientGrid.direction = event.direction;
    this.GetHCBClientGrid(this.getHCBClientGrid);
  }

  Filter(): void { 
    this.pageIndex=0;
    this.getHCBClientGrid.pageIndex=0;
    this.getHCBClientGrid.ClaimantDOB = (this.filterDrp.value.clientDOB == "" || this.filterDrp.value.clientDOB == null) ? "" : "" + this.filterDrp.value.clientDOB._i.year + "-" + (this.filterDrp.value.clientDOB._i.month + 1) + "-" + this.filterDrp.value.clientDOB._i.date + " 00:00:00:000";
    this.getHCBClientGrid.HCBReferenceNo = this.filterDrp.value.refNo;
    this.getHCBClientGrid.ClientId = this.filterDrp.value.clientName;
   // this.getHCBClientGrid.ClientContact = this.filterDrp.value.clientContact;
    this.getHCBClientGrid.AssessorId = this.filterDrp.value.AssessorId
    this.getHCBClientGrid.CaseMgrId = this.filterDrp.value.CaseMgrId;
    this.getHCBClientGrid.ClaimantFirstName=this.filterDrp.value.ClaimantFirstName;
    this.getHCBClientGrid.ClaimantLastName=this.filterDrp.value.ClaimantLastName;
    this.getHCBClientGrid.HCBReceivedDate=(this.filterDrp.value.hcbReceivedDate== "" || this.filterDrp.value.hcbReceivedDate == null) ? "" : "" + this.filterDrp.value.hcbReceivedDate._i.year + "-" + (this.filterDrp.value.hcbReceivedDate._i.month + 1) + "-" + this.filterDrp.value.hcbReceivedDate._i.date + " 00:00:00:000";;
    this.GetHCBClientGrid(this.getHCBClientGrid);
  }

  Clear(): void {
    this.filterDrp.reset();
    this.pageIndex=0;
    this.getHCBClientGrid.pageIndex=0;
    this.getHCBClientGrid.ClaimantDOB = this.filterDrp.value.clientDOB;
    this.getHCBClientGrid.HCBReferenceNo = this.filterDrp.value.refNo;
    this.getHCBClientGrid.ClientId = this.filterDrp.value.clientName;
    //this.getHCBClientGrid.ClientContact = this.filterDrp.value.clientContact;
    this.getHCBClientGrid.AssessorId = this.filterDrp.value.AssessorId;
    this.getHCBClientGrid.CaseMgrId = this.filterDrp.value.CaseMgrId;
    this.getHCBClientGrid.ClaimantFirstName=this.filterDrp.value.ClaimantFirstName;
    this.getHCBClientGrid.ClaimantLastName=this.filterDrp.value.ClaimantLastName;
    this.getHCBClientGrid.HCBReceivedDate=this.filterDrp.value.hcbReceivedDate;
    this.GetHCBClientGrid(this.getHCBClientGrid);
  }
}
