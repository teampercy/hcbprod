import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../Material.module';
import { AuthGuard, AuthGuard2 } from 'app/custom/auth.guard';
import { GoogleChartsModule } from 'angular-google-charts';
import{ImportCasesComponent} from './import-cases.component';

const routes = [
  {
      path     : 'import-cases',
      component: ImportCasesComponent,
      canActivate: [AuthGuard2]
  }
];

@NgModule({
  declarations: [
    ImportCasesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    FuseSharedModule,
    MaterialModule,
    DateRangePickerModule,
    FuseWidgetModule,
    NgxChartsModule,
    GoogleChartsModule
  ],
  entryComponents: [


   ]
})
export class ImportCasesModule { }
