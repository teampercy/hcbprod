import { NgModule } from '@angular/core';
import { CrmDashboardComponent } from './crm-dashboard.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/main/Material.module';
import { AuthGuard } from 'app/custom/auth.guard';
import { FuseWidgetModule } from '@fuse/components';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import { OpenCloseCaseClientComponent } from './open-close-case-client/open-close-case-client.component';
import { OpenCloseCaseMgrComponent } from './open-close-case-mgr/open-close-case-mgr.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { OpenClientListComponent } from './open-client-list/open-client-list.component';
import { ActiveInactiveClientComponent } from './active-inactive-client/active-inactive-client.component';

const routes = [
  {
      path     : 'crm-dashboard',
      component: CrmDashboardComponent,
      canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [
    CrmDashboardComponent,
    OpenCloseCaseClientComponent,
    OpenCloseCaseMgrComponent,
    OpenClientListComponent,
    ActiveInactiveClientComponent
  ],
  imports     : [
      RouterModule.forChild(routes),
      TranslateModule,
      FuseSharedModule,
      MaterialModule,
      FuseWidgetModule,
      NgxChartsModule,
      DateRangePickerModule,
      GoogleChartsModule
  ],
  entryComponents: [
    OpenCloseCaseClientComponent,
    OpenCloseCaseMgrComponent,
    OpenClientListComponent,
    ActiveInactiveClientComponent
   ]
})
export class CrmDashboardModule { }
