import { Component, OnInit, ViewEncapsulation, Inject, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatDialogRef, MAT_DIALOG_DATA, MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
 import { Router } from '@angular/router';
@Component({
  selector: 'app-open-close-case-mgr',
  templateUrl: './open-close-case-mgr.component.html',
  styleUrls: ['./open-close-case-mgr.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class OpenCloseCaseMgrComponent implements OnInit {
  errorText: string;
  Working: boolean;
  dataSource: any;
  dialogTitle: string;

  displayedColumns: string[] = ['HCBReferenceNo', 'CompanyName','CaseManager', 'HCBReceivedDate', 'CaseClosedDate', 'Id'];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _router: Router,
    public dialogRef: MatDialogRef<OpenCloseCaseMgrComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    if (this.data.Id == 1) {
      this.dialogTitle = "Open Cases"
    } else {
      this.dialogTitle = "Closed Cases"
    }
    this.dataSource = new MatTableDataSource<any>(this.data.caseMgr);

    this.dataSource.paginator = this.paginator;
  }

  
  Edit(hcbReferenceId: number): void {
      
    this._router.navigate(["/case-managment/addedit-case-managment/edit", hcbReferenceId]);

}

}
