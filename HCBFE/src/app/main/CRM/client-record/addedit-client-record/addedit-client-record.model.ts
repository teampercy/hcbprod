export class GetCRMClientModel{
    ClientID:number;
    ClientAddedDate:Date;
    ClientTypeId:number;
    CompanyName:string; 
    RegionId:bigint;
    HeadOfficeAddress:string;
    Town:string;
    Postcode:string;
    LandlineNo:string;
    MobileNo:string;
    PrincipalContactName:string;
    PrincipalContactNo:string;
    PrincipalContactEmail:string;
    BrokerId:number;
    PensionScheme:string;
    FinalSalary:boolean;
    IllHealthEarlyRetirement:boolean;
    IPInsurerName:string;
    DeferredPeriod:string;
    Duration:string;
    RenewalDue:string;
    DefinitionOfOccupation:string;
    MIInsurerAdminName:string; 
    CPInsurerName:string;
    EAPProviderName:string; 
    DaysPaidSickLeave:number;
    OHPprovider:string;
    Recording:string; 
    RelationshipOwner:string;
    MeetingIntervals:string;
    FormalContractReqd:boolean;
    ContractSignedDate:Date;
    ContractExpiryDate:Date;
    ConsentToUseLogo:boolean;
    Status:boolean; 
    ClientServices:string;
}


export class SaveCRMClientModel{
    ClientAddedDate:Date;
    ClientTypeId:number;
    CompanyName:string; 
    RegionId: bigint;
    HeadOfficeAddress:string;
    Town:string;
    Postcode:string;
    LandlineNo:string;
    MobileNo:string;
    PrincipalContactName:string;
    PrincipalContactNo:string;
    PrincipalContactEmail:string;
    BrokerId:number;
    PensionScheme:string;
    FinalSalary:boolean;
    IllHealthEarlyRetirement:boolean;
    IPInsurerName:string;
    DeferredPeriod:string;
    Duration:string;
    RenewalDue:string;
    DefinitionOfOccupation:string;
    MIInsurerAdminName:string; 
    CPInsurerName:string;
    EAPProviderName:string; 
    DaysPaidSickLeave:number;
    OHPprovider:string;
    Recording:string; 
    RelationshipOwner:string;
    MeetingIntervals:string;
    FormalContractReqd:boolean;
    ContractSignedDate:string;
    ContractExpiryDate:string;
    NextActionDate:string;
    ConsentToUseLogo:boolean;
    Status:boolean; 
    CreatedBy:number;
    ClientServices:string;
    SendNotificationEmail:boolean;
    UserName:string;
   
}

export class UpdateCRMClientModel{
    ClientID:number;
    ClientAddedDate:Date;
    ClientTypeId:string;
    CompanyName:string; 
    RegionId:bigint;
    HeadOfficeAddress:string;
    Town:string;
    Postcode:string;
    LandlineNo:string;
    MobileNo:string;
    PrincipalContactName:string;
    PrincipalContactNo:string;
    PrincipalContactEmail:string;
    BrokerId:number;
    PensionScheme:string;
    FinalSalary:boolean;
    IllHealthEarlyRetirement:boolean;
    IPInsurerName:string;
    DeferredPeriod:string;
    Duration:string;
    RenewalDue:string;
    DefinitionOfOccupation:string;
    MIInsurerAdminName:string; 
    CPInsurerName:string;
    EAPProviderName:string; 
    DaysPaidSickLeave:number;
    OHPprovider:string;
    Recording:string; 
    RelationshipOwner:string;
    MeetingIntervals:string;
    FormalContractReqd:boolean;
    ContractSignedDate:string;
    ContractExpiryDate:string;
    ConsentToUseLogo:boolean;
    Status:boolean; 
    LastModifiedBy:number;
    ClientServices:string;
    NextActionDate:string;

}

export class  SaveUpdateClientInfo{
ID:number;
    ClientId:number;
    DateOfInformation:string;
    AgreedSLAId:number;
    DefinedSLA: string;
    ReportingFormat: number;
    EncryptedEmailAddress:string;
    IsTLSEnabled :number;
    TLSDate: string;
    EncryptedPassword:string;
    InvoiceFrequency: number;
    SpecificInstruction:string;
          IsOrederRequired:number;
        PONumber: string;
        DatePOExpiry: string;

}

export class DocumentGrid {
    ClientID: number;
    DocumentName: string;
    DocumentPath: string;
    UploadedBy: number;
    DateCreated: Date;
}
export class DocumentUpload {
    ClientId: number;
}

export class Note {
    ClientId: number;
    fullName:string;
    noteCreatedOn:Date;
    description:string;
    NextActionDate:string;
    displayMessage:string;
}

export class ReportEmailList{
    id: number;
    ClientId: number;
    emailAddress:string;
    checkEmailAddress:boolean;
}

export class GetClientServices {
    ServiceTypeId: number;
    ServiceType: string;
    ClientTypeId: string;
   
}