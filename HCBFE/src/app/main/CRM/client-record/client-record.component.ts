import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort, PageEvent, Sort, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { ClientRecordService } from './client-record.service';
import { CRMClientRecordGrid, CRMClientRecord, GetCRMClientGrid } from './client-record.model';
import * as _moment from 'moment';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
const moment = _moment;
@Component({
  selector: 'app-client-record',
  templateUrl: './client-record.component.html',
  styleUrls: ['./client-record.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class ClientRecordComponent implements OnInit {

  FirstGroup: FormGroup;
  errorText: string;
  TotalRecords: number;
  pageIndex: number;
  pageSize: number;
  sortColumn: string;
  sortDirection: 'asc' | 'desc' | '';
  getCrmClientRecordGrid: GetCRMClientGrid;
  FilterClientRecord: FormGroup;
  crmClientRecord: any;
  working: boolean;
  displayedColumns: string[] = ['CompanyName', 'ClientType', 'ClientAddedDate', 'PrincipalContactName', 'PrincipalContactNumber', 'Id'];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private _router: Router,
    private _formbuilder: FormBuilder,
    private _clientRecord: ClientRecordService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.pageIndex = 0;
    this.pageSize = 15;
    this.sortColumn = 'CompanyName';
    this.sortDirection = 'asc';

    this.getCrmClientRecordGrid = {
      pageIndex: this.pageIndex,
      pageSize: 15,
      sort: this.sortColumn,
      direction: this.sortDirection,
      // ClientType: null,
      clientAddedDate: null,
      CompanyName: null,
      ClientTypeId: null
    }

    this.GetCRMClientGrid(this.getCrmClientRecordGrid);

    this.FilterClientRecord = this._formbuilder.group({
      CompanyName: [''],
      ClientTypeId: [],
      ClientAddedDate: ['']
    });

  }

  // ngAfterViewInit() {
  //   alert("ngafter");
  //   this.crmClientRecord.paginator = this.paginator;
  //  }

  GetCRMClientGrid(model: GetCRMClientGrid): void {
    this.crmClientRecord=[];
    this.paginator=null;
    this._clientRecord.GetCRMClientRecordGrid(model)
      .subscribe((response: CRMClientRecord) => {
        this.crmClientRecord = new MatTableDataSource<CRMClientRecordGrid>(response['result'].crmClientRecordGrid);
        this.TotalRecords = response['result'].crmClientRecordLength[0].totalLength;
        response['result'].crmClientRecordGrid.forEach(childObj => {
          if (childObj.clientTypeId == 1) {
            childObj.ClientType = 'Insurance'
          }
          else {
            childObj.ClientType = 'Retail'
          }
        });

        this.crmClientRecord.sort = this.sort;
        this.crmClientRecord.paginator = this.paginator;


      }, (error: any) => {
        this.errorText = error;
      });
  }


  AddClientRecord() {
    this._router.navigate(['/client-record/addedit-client-record/add']);
  }

  EditClientRecord(ClientID: number): void {
    this._router.navigate(["/client-record/addedit-client-record/edit", ClientID]);
  }

  DeleteClientRecord(ClientID: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px', data: { dispalymessage: 'Are you sure you want to delete this Item?' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._clientRecord.DeleteClientRecord(ClientID)
          .subscribe((response: any) => {
            console.log(response['result']['table'][0].result);
            if (response['result']['table'][0].result==0)
            {
              this.dialog.open(SuggestionComponent, { width: '450px', height: '180px',  data: { dispalymessage: 'Client cannot be deleted as Case Records exist for this Client' } });
            }
            else{
              this.ConfirmDialog();
              this.GetCRMClientGrid(this.getCrmClientRecordGrid);
            }
           
          }, (error: any) => {
            if (error == null) {
              this.errorText = 'Internal Server error';
            }
            else {
              this.errorText = error;
            }
          });
      }
    });
  }


  pageChanged(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getCrmClientRecordGrid.pageIndex = event.pageIndex;
    this.getCrmClientRecordGrid.pageSize = event.pageSize;
    this.GetCRMClientGrid(this.getCrmClientRecordGrid);
  }

  sortChange(event: Sort): void {
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getCrmClientRecordGrid.sort = event.active;
    this.getCrmClientRecordGrid.direction = event.direction;
    this.GetCRMClientGrid(this.getCrmClientRecordGrid);
  }

  Filter(): void {
    this.pageIndex=0;
    this.getCrmClientRecordGrid.pageIndex=0;
    this.getCrmClientRecordGrid.clientAddedDate = (this.FilterClientRecord.value.ClientAddedDate == "" || this.FilterClientRecord.value.ClientAddedDate == null) ? "" : "" + this.FilterClientRecord.value.ClientAddedDate._i.year + "-" + (this.FilterClientRecord.value.ClientAddedDate._i.month + 1) + "-" + this.FilterClientRecord.value.ClientAddedDate._i.date + " 00:00:00:000";
    this.getCrmClientRecordGrid.CompanyName = this.FilterClientRecord.value.CompanyName;
    // this.getCrmClientRecordGrid.clientAddedDate = this.FilterClientRecord.value.ClientAddedDate;
    this.getCrmClientRecordGrid.ClientTypeId = this.FilterClientRecord.value.ClientTypeId;
    this.GetCRMClientGrid(this.getCrmClientRecordGrid);
  }

  Clear(): void {
    this.pageIndex=0;
    this.getCrmClientRecordGrid.pageIndex=0;
    this.FilterClientRecord.reset();
    this.getCrmClientRecordGrid.CompanyName = this.FilterClientRecord.value.CompanyName;
    this.getCrmClientRecordGrid.clientAddedDate = this.FilterClientRecord.value.clientAddedDate;
    this.getCrmClientRecordGrid.ClientTypeId = this.FilterClientRecord.value.ClientTypeId;
    this.GetCRMClientGrid(this.getCrmClientRecordGrid);
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }

}

