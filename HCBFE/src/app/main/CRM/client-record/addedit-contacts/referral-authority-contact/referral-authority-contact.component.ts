import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ClientRecordService } from '../../client-record.service';
import { GetReferralContact, UpdateReferralContact } from './referral-authority-contact.model';

@Component({
  selector: 'app-referral-authority-contact',
  templateUrl: './referral-authority-contact.component.html',
  styleUrls: ['./referral-authority-contact.component.scss'],
  animations: [
    fuseAnimations
  ],
  encapsulation: ViewEncapsulation.None
})
export class ReferralAuthorityContactComponent implements OnInit {

  ReferralAuthorityContact: FormGroup;
  errorText:string;
  working:boolean;
  dialog:any;

  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _route: ActivatedRoute,
    private _clientRecord: ClientRecordService,
    private _matSnackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ReferralAuthorityContactComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  
    if (this.data.ClientReferralContactsId) {
      this.ReferralAuthorityContact = this._formbuilder.group({
        ClientReferralContactsId: [this.data.ClientReferralContactsId],
        clientId: [this.data.ClientId],
        contactName: ['',[Validators.required]],
        jobTitle: [''],
        phoneNo: ['', [Validators.maxLength]],
        nonHOLocation: [''],
        contactEmail: ['']
      });
      this.SetReferralContact();
    }
    else if(this.data.ClientReferralContactsId == undefined)
    {
      this.ReferralAuthorityContact = this._formbuilder.group({
        clientId: [this.data.ClientId],
        contactName: ['',[Validators.required]],
        jobTitle: [''],
        phoneNo: ['', [Validators.maxLength]],
        nonHOLocation: [''],
        contactEmail: ['']
      });
        //  this.SetValueForUndefined();
    }
    else {
      this.ReferralAuthorityContact = this._formbuilder.group({
        clientId: [this.data.ClientId],
        contactName: ['',[Validators.required]],
        jobTitle: [''],
        phoneNo: [''],
        nonHOLocation: [''],
        contactEmail: ['']
      });
     
    }

  }
 

  SetReferralContact():void {
   
    this._clientRecord.GetReferralContact(this.data.ClientReferralContactsId)
      .subscribe((response: GetReferralContact) => {
      
        this.ReferralAuthorityContact.patchValue({ contactName: response['result'][0].contactName });

        this.ReferralAuthorityContact.patchValue({ jobTitle: response['result'][0].jobTitle });
        this.ReferralAuthorityContact.patchValue({ phoneNo: response['result'][0].phoneNo });
        this.ReferralAuthorityContact.patchValue({ nonHOLocation: response['result'][0].nonHOLocation });
        this.ReferralAuthorityContact.patchValue({ contactEmail:response['result'][0].contactEmail });
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      })
  }

  Save(): void {
 
    const updateReferralContact: UpdateReferralContact = Object.assign({}, this.ReferralAuthorityContact.value);
    updateReferralContact.ClientId=this.data.ClientId;

    if(this.data.ClientId){
    if (this.data.ClientReferralContactsId) {
      this.working = true;
      this._clientRecord.UpdateReferralContact(updateReferralContact)
        .subscribe((response: number) => {
         this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.working=false;
        });
        
    } else {
      this.working = true;
      
     // this.dialogRef.close(true);
        this._clientRecord.SaveReferralContact(updateReferralContact)
         .subscribe((response: number) => {    
           this.dialogRef.close(true);
          
         }, (error: any) => {
           this.errorText = error;
          });
    }
  }
  else{
    this.data.refferalContactGrid.push(updateReferralContact);
    this.dialogRef.close(true);
    this.working = false;
  }
  }

  
  SetFormat(event): boolean {
    return this._clientRecord.ValidatePhoneFormat(event);
  }



}
