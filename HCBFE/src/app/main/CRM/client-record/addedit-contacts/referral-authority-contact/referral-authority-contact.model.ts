export class GetReferralContact
{
    ClientReferralContactsId:number;
    ClientId:number;
    ContactName:string;
    JobTitle:string;
    PhoneNo:string;
    NonHOLocation:string;
    ContactEmail:string;
}

export class SaveReferralContact
{
    ClientId:number;
    ContactName:string;
    JobTitle:string;
    PhoneNo:string;
    NonHOLocation:string;
    ContactEmail:string;
}

export class UpdateReferralContact
{
   ClientReferralContactsId:number;
   ClientId:number; 
   ContactName:string;
   JobTitle:string;
   PhoneNo:string; 
   NonHOLocation:string; 
   ContactEmail:string;
}