import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ClientRecordService } from '../../client-record.service';
import { GetAccountContact, UpdateAccountContact } from './account-contact.model';

@Component({
  selector: 'app-account-contact',
  templateUrl: './account-contact.component.html',
  styleUrls: ['./account-contact.component.scss'],
  animations: [
    fuseAnimations
  ],
  encapsulation: ViewEncapsulation.None
})
export class AccountContactComponent implements OnInit {

  AccountContact: FormGroup;
  errorText: string;
  working: boolean;
  dialog: any;

  constructor(
    private _formbuilder: FormBuilder,
    private _router: Router,
    private _clientRecord: ClientRecordService,
    private _matSnackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AccountContactComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.ClientAccountContactsId) {
      this.AccountContact = this._formbuilder.group({
        ClientAccountContactsId: [this.data.ClientAccountContactsId],
        ClientId: [this.data.ClientId],
        fullName: ['',[Validators.required]],
        telephoneNumber: ['', Validators.maxLength],
        instructions: [this.data.PONumber],
        emailAddress: [''],
        paymentTerms: ['']
      });
      this.SetAccountContact();
    }
    else {
      this.AccountContact = this._formbuilder.group({
        clientId: [this.data.ClientId],
        fullName: ['',[Validators.required]],
        telephoneNumber: ['',[Validators.maxLength]],
        instructions: [this.data.PONumber],
        emailAddress: [''],
        paymentTerms: ['']
      });
    }
  }

  SetAccountContact(): void {
    
    this._clientRecord.GetAccountContact(this.data.ClientAccountContactsId)
      .subscribe((response: GetAccountContact) => {
        this.AccountContact.patchValue({ fullName: response['result'][0].fullName });
        this.AccountContact.patchValue({ telephoneNumber: response['result'][0].telephoneNumber });
        this.AccountContact.patchValue({ instructions: this.data.PONumber });
        this.AccountContact.patchValue({ emailAddress: response['result'][0].emailAddress });
        this.AccountContact.patchValue({ paymentTerms: response['result'][0].paymentTerms });
        this.working = false;
      }, (error: any) => {
        this.errorText = error;
        this.working = false;
      })
  }

  Save(): void {
 
    const updateAccountContact: UpdateAccountContact = Object.assign({}, this.AccountContact.value);
    updateAccountContact.clientId = this.data.ClientId;
if(this.data.ClientId)
{
    if (this.data.ClientAccountContactsId) {
      this.working = true;
      this._clientRecord.UpdateAccountContact(updateAccountContact)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
          this.working = false;
        });
  }else
  {
     this.working = true;
     // this.data.accountContactGrid.push(updateAccountContact);
      // this.dialogRef.close(true);
      this._clientRecord.SaveAccountContact(updateAccountContact)
        .subscribe((response: number) => {
          this.dialogRef.close(true);
        }, (error: any) => {
          this.errorText = error;
          this.errorMessage(this.errorText);
        });
  }
}
  
 else{
  this.data.accountContactGrid.push(updateAccountContact);
   updateAccountContact.Id = (this.data.accountContactGrid.length)-1;
   this.dialogRef.close(true);
    this.working = false;
 }
//}
 
}
  errorMessage(errortext) {
    this._matSnackBar.open(errortext, 'x', {
      verticalPosition: 'top',
      duration: 5000,
      panelClass: ['snackbarError']
    });
  }
  SetFormat(event): boolean {
    return this._clientRecord.ValidatePhoneFormat(event);
  }


}
