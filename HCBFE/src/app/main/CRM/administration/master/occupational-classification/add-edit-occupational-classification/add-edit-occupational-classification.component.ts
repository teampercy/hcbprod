import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MasterService } from '../../master.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SaveMasterList } from '../../master.model';

@Component({
  selector: 'app-add-edit-occupational-classification',
  templateUrl: './add-edit-occupational-classification.component.html',
  styleUrls: ['./add-edit-occupational-classification.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class AddEditOccupationalClassificationComponent implements OnInit {

  Count:number;
  name:string;
  NameTaken:boolean = false;
  errorText: string;
  working: boolean;
  TypeId:number;
  OccupationalClassification: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditOccupationalClassificationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.OccupationalClassificationId > 0) {
      this.OccupationalClassification = this._formBuilder.group({
        Id: [this.data.OccupationalClassificationId],
        TypeId: [26],
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        RelatedValue:[null]
      })
      this.SetValues();
    } else {
      this.OccupationalClassification = this._formBuilder.group({
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TypeId: [26],
        RelatedValue:[null]
      })
    }
    this.TypeId=this.OccupationalClassification.value.TypeId;
  }

  createvalidators(_master: MasterService) {
    return control => new Promise((resolve: any) => {
  
      if (this.name == control.value || this.name == "") {
        resolve(null);
      }
      else{
        this._master.CheckMasterList(control.value,this.TypeId)
          .subscribe((response) => {
          //  console.log(response);
            this.Count = response['result'].masterName[0].resultCount;
          //  console.log(this.Count);
            if (this.Count != 0) {
               resolve({ NameTaken: true });
  
            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }

  SetValues(): void {
    this._master.GetMasterListValueById(this.data.OccupationalClassificationId)
      .subscribe((response: any) => {
        this.name= response['result'][0].name;
        this.OccupationalClassification.patchValue({ Name: response['result'][0].name });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  SaveOccupationalClassification(): void {
    if (this.OccupationalClassification.invalid)
      return;

    this.working = true;
    if (this.data.OccupationalClassificationId) {
      const updateOccupationalQualification: SaveMasterList = Object.assign({}, this.OccupationalClassification.value);

      this._master.UpdateMasterListValue(updateOccupationalQualification)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
      this.working = true;
      this._master.SaveMasterListValue(this.OccupationalClassification.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }
}
