import { Injectable } from '@angular/core';
import { HttpClientsService } from 'app/service/http-client.service';
import { AuthenticationService } from 'app/service/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetddlTypelistValues } from 'app/main/HCB/case-managment/addedit-case-managment/addedit-case-management.model';
import { SaveMasterList, updateAssessor ,GetSPCompanyGrid,AddEditSPCompany } from './master.model';


@Injectable({
  providedIn: 'root'
})
export class MasterService {

  constructor(private _http: HttpClientsService,
    private _authentication: AuthenticationService,
    private http: HttpClient) { }

  GetddlInacpacityDefination(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetddlDeferredPeriod(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetddlSchemeName(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetddlIllnessInjury(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetddlFeesPayable(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }

  GetddlClaimClosedReason(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetddlReasonClosed(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetddlTypeOfContact(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetddlRegion(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  GetMasterTypelistValues(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }

  GetddlOccupationalClassification(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }

  GetddlValues(TypeId: number):Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }

  GetMasterListValueById(Id: number): Observable<any | {}> {
  
    return this._http.getAPI("Master/GetMasterListValueById?Id=" + Id);
  }

  SaveMasterListValue(model: SaveMasterList): Observable<number | {}> {
    return this._http.postAPI("Master/SaveMasterListValue", model);
  }

  UpdateMasterListValue(model: SaveMasterList): Observable<number | {}> {
    return this._http.postAPI("Master/UpdateMasterListValue", model);
  }

  deleteMasterListValueById(Id: number,Table:string,TypeId:number,UserName:string): Observable<number | {}> {
    return this._http.getAPI("Master/deleteMasterListValueById?Id=" + Id+"&Table="+Table+"&TypeId="+TypeId+"&UserName="+UserName);
  }

  deleteRelationshipOwnerById(Id: number): Observable<number | {}> {
    return this._http.getAPI("Master/deleteRelationshipOwnerById?Id=" + Id);
  }
  deleteBrokerById(Id: number): Observable<number | {}> {
    return this._http.getAPI("Master/deleteBrokerById?Id=" + Id);
  }
  GetddlServiceRequired(TypeId: number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }
  
  GetNoActivityInterval(TypeId:number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }

  GetddlHealthTrust(TypeId:number): Observable<GetddlTypelistValues | {}> {
    return this._http.getAPI("Master/GetddlTypelistValues?TypeId=" + TypeId);
  }

  GetAssessorGrid() {
    return this._http.getAPI("Master/GetAssessorList");
  }
  GetAssessorDetailById(Id: number) {
    return this._http.getAPI("Master/GetAssessorDeatailById?Id=" + Id);
  }
  UpdateAssessorDetail(model: updateAssessor) {
    return this._http.postAPI("Master/UpdateAssessorDeatail", model);
  }
  SaveAssessorDeatail(model: updateAssessor) {
    return this._http.postAPI("Master/AddAssessorDeatail", model);
  }
  deleteAssessor(Id: number): Observable<number | {}> {
    return this._http.getAPI("Master/deleteAssessor?Id=" + Id);
  }
  MasterListdeleteOrNot(Id: number): Observable<number | {}> {
    return this._http.getAPI("Master/MasterListdeleteOrNot?Id=" + Id);
  }
  CheckMasterList(Name: string,TypeId:number) {
    return this._http.getAPI("Master/CheckMasterList?Name=" + Name+"&TypeId="+TypeId);
  }
  CheckAssessorExistOrNot(Name: string) {
    return this._http.getAPI("Master/CheckAssessorExistOrNot?Name=" + Name);
  }
  GetServiceProviderCompany(): Observable<GetSPCompanyGrid[]>  {
    return this._http.getAPI("Master/GetServiceProviderCompany");
  }
  UpdateSPCompany(model:AddEditSPCompany)
  {
    return this._http.postAPI("Master/UpdateSPCompany",model);
  }
  AddSPCompany(model:AddEditSPCompany)
  {
    return this._http.postAPI("Master/AddSPCompany",model);
  }

  GetSPCompanyDetailById(Id:number)
  {
    return this._http.getAPI("Master/GetSPCompanyDetailById?CompanyId=" + Id);
  }
  SPCompanyDeleteOrNot(Id:number)
  {
    return this._http.getAPI("Master/SPCompanyDeleteOrNot?Id=" + Id);
  }
  DeleteSPcompany(Id:number)
  {
    return this._http.getAPI("Master/DeleteSPcompany?Id=" + Id);
  }
  CheckSPCompanyExist(Name: string,CompanyId:number) {
    return this._http.getAPI("Master/CheckSPCompanyExist?Name=" + Name+ "&CompanyId=" +CompanyId);
  }

  ValidatePhoneFormat(event: any): boolean {
    let output: boolean = true;
    if (event.key === "0" || event.key === "1" || event.key === "2" || event.key === "3" || event.key === "4"
      || event.key === "5" || event.key === "6" || event.key === "7" || event.key === "8" || event.key === "9" || event.key === "+" || event.key === "("
      || event.key === ")" || event.key === "Delete" || event.key === "Backspace" || event.key === "ArrowLeft" || event.key === "ArrowRight" || event.keyCode === "TABKEY"
    ) {
      output = true;
    } else {
      output = false;
    }
    return output;
  }

}
