import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClientRecordService } from 'app/main/CRM/client-record/client-record.service';
import { MasterService } from '../master.service';
import { ActivatedRoute } from '@angular/router';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { AddEditBrokersComponent } from 'app/main/CRM/client-record/add-edit-brokers/add-edit-brokers.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';

@Component({
  selector: 'app-brokers',
  templateUrl: './brokers.component.html',
  styleUrls: ['./brokers.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class BrokersComponent implements OnInit {
  working: boolean;
  errorText: string;
  dialogRef: any;
  Filterbrokers: FormGroup;
  filterValue: string;
  BrokerData: any;
  displayedColumns = ['FullName','EmailAddress', 'Id', 'delete'];

  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _clientRecord: ClientRecordService,
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.Filterbrokers = this._formbuilder.group({
      BrokerName: ['']
    });
    this.GetBrokergrid();
  }

  GetBrokergrid(): void {
    this._clientRecord.GetBroker()
      .subscribe((response: any) => {
        this.BrokerData = new MatTableDataSource<any>(response['result']);
        this.BrokerData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        this.BrokerData.sort = this.sort;
        this.BrokerData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddRelationshipOwner(): void {
    this.dialogRef = this.dialog.open(AddEditBrokersComponent, {
      disableClose: true,
      width: '400px',
      data: {
        BrokerId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetBrokergrid();
      }
    });
  }

  EditBroker(BrokerId: number): void {
   
    this.dialogRef = this.dialog.open(AddEditBrokersComponent, {
      disableClose: true,
      width: '400px',
      data: {
        BrokerId: BrokerId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetBrokergrid();
      }
    });
  }

  DeleteBroker(Id: number): void {
   

        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteBrokerById(Id)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetBrokergrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });

    }
  

  applyFilter() {
    this.filterValue = this.Filterbrokers.get('BrokerName').value;
    this.BrokerData.filter = this.filterValue.trim().toLowerCase();

    if (this.BrokerData.paginator) {
      this.BrokerData.paginator.firstPage();
    }
  }
  ClearFilter(): void {
    this.Filterbrokers.reset();
    this.GetBrokergrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }

}
