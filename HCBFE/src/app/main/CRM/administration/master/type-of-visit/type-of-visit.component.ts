import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditTypeOfVisitComponent } from './add-edit-type-of-visit/add-edit-type-of-visit.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';

@Component({
  selector: 'app-type-of-visit',
  templateUrl: './type-of-visit.component.html',
  styleUrls: ['./type-of-visit.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class TypeOfVisitComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  typeOfVisitData: any;
  FilterTypeOfVisit: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterTypeOfVisit = this._formbuilder.group({
      Visit: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetVisitGrid();
  }

  GetVisitGrid(): void {
    this._masterService.GetMasterTypelistValues(15)
      .subscribe((response: any) => {
        this.typeOfVisitData = new MatTableDataSource<any>(response['result']);
        this.typeOfVisitData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        // this.illnessInjuryData.sort = this.sort;
        this.typeOfVisitData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddVisit(): void {
     this.dialogRef = this.dialog.open(AddEditTypeOfVisitComponent, {
       disableClose: true,
       width: '400px',
       data: {
         VisitId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetVisitGrid();
       }
     });
   }

   EditVisit(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditTypeOfVisitComponent, {
       disableClose: true,
    width: '400px',
       data: {
        VisitId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetVisitGrid();
      }
     });
   }

   DeleteVisit(Id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Are you sure you want to delete this Item?'}  });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._masterService.deleteMasterListValueById(Id,'MasterList',15,this.UserName)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetVisitGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }


  applyFilterVisit() {
     ;
    this.filterValue = this.FilterTypeOfVisit.get('Visit').value;
    this.typeOfVisitData.filter = this.filterValue.trim().toLowerCase();

    if (this.typeOfVisitData.paginator) {
      this.typeOfVisitData.paginator.firstPage();
    }
  }

  ClearVisit(): void {
    this.FilterTypeOfVisit.reset();
    this.GetVisitGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}

