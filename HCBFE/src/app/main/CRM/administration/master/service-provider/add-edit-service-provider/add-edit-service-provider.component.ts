import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MasterService } from '../../master.service';
import { AddEditSPCompany } from '../../master.model';
import { promise } from 'selenium-webdriver';
import { resolve } from 'q';

@Component({
  selector: 'app-add-edit-service-provider',
  templateUrl: './add-edit-service-provider.component.html',
  styleUrls: ['./add-edit-service-provider.component.scss']
})
export class AddEditServiceProviderComponent implements OnInit {

  Count: number;
  name: string;
  NameTaken: boolean = false;
  heading: string;
  errorText: string;
  working: boolean;
  SPcompanyDetails: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditServiceProviderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.heading = this.data.heading;
    if (this.data.SpCompanyId > 0) {
      this.SPcompanyDetails = this._formBuilder.group({
        SpCompanyId: [this.data.SpCompanyId],
        // TypeId: [5],
        SpCompanyName: ['', Validators.required,[Validators.composeAsync([this.createvalidators(this._master)])]]

      })
      this.SetValues();
    } else {
      this.SPcompanyDetails = this._formBuilder.group({
        SpCompanyId: [this.data.SpCompanyId],
        // TypeId: [5],
        SpCompanyName: ['', Validators.required,[Validators.composeAsync([this.createvalidators(this._master)])]]
      })
    }
  }


  createvalidators(_master: MasterService) {
    return control => new Promise((resolve: any) => {

      let SPCompanyName = control.value.replace(/[^\w\s]/gi, '%26').toLocaleLowerCase();
      SPCompanyName = SPCompanyName.trim();
      if (this.name == control.value || this.name == "") {
        resolve(null);

      }
      else {
        this._master.CheckSPCompanyExist(SPCompanyName, this.data.SpCompanyId)
          .subscribe((response) => {
            debugger;
            // console.log(response);
            this.Count = response['result'][0].resultCount;
            // console.log(this.Count);
            if (this.Count != 0) {
              resolve({ NameTaken: true });

            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }


  SetValues(): void {
    this._master.GetSPCompanyDetailById(this.data.SpCompanyId)
      .subscribe((response: any) => {
        this.name = response['result'][0].spCompanyName;
        this.SPcompanyDetails.patchValue({ SpCompanyName: response['result'][0].spCompanyName });
        // this.SPcompanyDetails.patchValue({ TeamEmailID: response['result'][0].teamEmailID });
        // this.SPcompanyDetails.patchValue({ TeamPhoneNo: response['result'][0].phoneNum });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  SetFormat(event): boolean {
    return this._master.ValidatePhoneFormat(event);
  }


  Savebroker(): void {
    if (this.SPcompanyDetails.invalid)
      return;

    this.working = true;
    if (this.data.SpCompanyId > 0) {
      const UpdateSPCompany: AddEditSPCompany = Object.assign({}, this.SPcompanyDetails.value);

      this._master.UpdateSPCompany(UpdateSPCompany)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
      this.working = true;
      this._master.AddSPCompany(this.SPcompanyDetails.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}

