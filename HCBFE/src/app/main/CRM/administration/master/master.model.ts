export class SaveMasterList {
    Id: number;
    TypeId:number;
    Name: string;
    RelatedValue:string;
}

export class updateAssessor{
     AssessorId: number;
     AssessorName: string;
     TeamPhoneNo:string;
     TeamEmailID:number;
}
export class GetSPCompanyGrid{
    SPCompanyId: number;
    SPCompanyName: string; 
}

export class AddEditSPCompany{
    SPCompanyId: number;
    SPCompanyName: string; 
}