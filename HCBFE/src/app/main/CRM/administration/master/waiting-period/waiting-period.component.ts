import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditWaitingPeriodComponent } from './add-edit-waiting-period/add-edit-waiting-period.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';

@Component({
  selector: 'app-waiting-period',
  templateUrl: './waiting-period.component.html',
  styleUrls: ['./waiting-period.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class WaitingPeriodComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  waitingPeriod: any;
  FilterWaitingPeriod: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterWaitingPeriod = this._formbuilder.group({
      WaitingPeriod: ['']
     });
    this.GetWaitingPeriodGrid();
  }

  GetWaitingPeriodGrid(): void {
    this._masterService.GetMasterTypelistValues(11)
      .subscribe((response: any) => {
        this.waitingPeriod = new MatTableDataSource<any>(response['result']);
        this.waitingPeriod.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        // this.illnessInjuryData.sort = this.sort;
        this.waitingPeriod.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddWaitingPeriod(): void {
     this.dialogRef = this.dialog.open(AddEditWaitingPeriodComponent, {
       disableClose: true,
       width: '400px',
       data: {
        PeriodId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetWaitingPeriodGrid();
       }
     });
   }

   EditWaitingPeriod(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditWaitingPeriodComponent, {
       disableClose: true,
    width: '400px',
       data: {
        PeriodId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetWaitingPeriodGrid();
      }
     });
   }

   DeleteWaitingPeriod(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {
      console.log(response['result'][0].countId);
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Record cannot be deleted due to integrity references'}});
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteMasterListValueById(Id,'MasterList',11,this.UserName)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetWaitingPeriodGrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })
  }


  applyFilterWaitingPeriod() {
    this.filterValue = this.FilterWaitingPeriod.get('WaitingPeriod').value;
    this.waitingPeriod.filter = this.filterValue.trim().toLowerCase();

    if (this.waitingPeriod.paginator) {
      this.waitingPeriod.paginator.firstPage();
    }
  }

  ClearwaitingPeroid(): void {
    this.FilterWaitingPeriod.reset();
    this.GetWaitingPeriodGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }


}
