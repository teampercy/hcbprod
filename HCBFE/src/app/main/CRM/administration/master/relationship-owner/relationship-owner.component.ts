import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatSort, MatPaginator, MatDialog, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClientRecordService } from 'app/main/CRM/client-record/client-record.service';
import { MasterService } from '../master.service';
import { ActivatedRoute } from '@angular/router';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { AddReferenceOwnerComponent } from 'app/main/CRM/client-record/add-reference-owner/add-reference-owner.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';

@Component({
  selector: 'app-relationship-owner',
  templateUrl: './relationship-owner.component.html',
  styleUrls: ['./relationship-owner.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})

export class RelationshipOwnerComponent implements OnInit {
  working: boolean;
  errorText: string;
  dialogRef: any;
  FilterRelOwner: FormGroup;
  filterValue: string;
  ownerData: any;
  displayedColumns = ['FullName','EmailAddress', 'Id', 'delete'];

  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _clientRecord: ClientRecordService,
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.FilterRelOwner = this._formbuilder.group({
      RelOwnerName: ['']
    });
    this.GetRelationShipOwnergrid();
  }

  GetRelationShipOwnergrid(): void {
    this._clientRecord.GetRelationshipOwner()
      .subscribe((response: any) => {
        this.ownerData = new MatTableDataSource<any>(response['result']);
        this.ownerData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
        this.ownerData.sort = this.sort;
        this.ownerData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddRelationshipOwner(): void {
    this.dialogRef = this.dialog.open(AddReferenceOwnerComponent, {
      disableClose: true,
      width: '400px',
      data: {
        OwnerId: 0
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetRelationShipOwnergrid();
      }
    });
  }

  EditRelationshipOwner(ownerId: number): void {
    this.dialogRef = this.dialog.open(AddReferenceOwnerComponent, {
      disableClose: true,
      width: '400px',
      data: {
        OwnerId: ownerId
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetRelationShipOwnergrid();
      }
    });
  }

  DeleteRelationshipOwner(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {
      console.log(response['result'][0].countId);
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Record cannot be deleted due to integrity references'}});
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteRelationshipOwnerById(Id)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetRelationShipOwnergrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })
  }

  applyFilter() {
    this.filterValue = this.FilterRelOwner.get('RelOwnerName').value;
    this.ownerData.filter = this.filterValue.trim().toLowerCase();

    if (this.ownerData.paginator) {
      this.ownerData.paginator.firstPage();
    }
  }
  ClearFilter(): void {
    this.FilterRelOwner.reset();
    this.GetRelationShipOwnergrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }

}
