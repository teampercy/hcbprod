import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientReferralSourceComponent } from './client-referral-source.component';

describe('ClientReferralSourceComponent', () => {
  let component: ClientReferralSourceComponent;
  let fixture: ComponentFixture<ClientReferralSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientReferralSourceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientReferralSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
