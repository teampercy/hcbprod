import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditBrokerComponent } from './add-edit-broker/add-edit-broker.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { SuggestionComponent} from 'app/main/Common/suggestion/suggestion.component';
import { navigation, navigationCRM } from 'app/navigation/navigation';

@Component({
  selector: 'app-broker',
  templateUrl: './broker.component.html',
  styleUrls: ['./broker.component.scss'],
  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class BrokerComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  AssessorData: any;
  FilterAssessor: FormGroup;
  filterValue: string;
  displayColumns = ['Name', 'Id', 'delete'];
  navigation:any;
  heading:string;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

   var queryStringValue = window.location.href.split('#')[0];
   if (queryStringValue.toLowerCase().indexOf("/crm") > -1) {   
   
        this.navigation = navigationCRM.map(x => Object.assign({}, x));  
       localStorage.setItem('Site', "2");
       this.heading="Broker";
        
    }
    else {
    
      this.navigation = navigation.map(x => Object.assign({}, x));
      localStorage.setItem('Site', "1");
      this.heading="Assessor";
   }


    this.FilterAssessor = this._formbuilder.group({
     AssessorName: ['']
     });
    this.GetBrokerGrid();
  }

  GetBrokerGrid(): void {
    this._masterService.GetAssessorGrid()
      .subscribe((response: any) => {

        this.AssessorData = new MatTableDataSource<any>(response['result']);
        this.AssessorData.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
   
        this.AssessorData.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddAssessor(): void {
    this.dialogRef = this.dialog.open(AddEditBrokerComponent, {
      disableClose: true,
      width: '400px',
      data: {
        AssesorId: 0,
        heading: this.heading
      }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ConfirmDialog();
        this.GetBrokerGrid();
      }
    });
  }


  EditBroker(Id: number): void {
    this.dialogRef = this.dialog.open(AddEditBrokerComponent, {
      disableClose: true,
      width: '400px',
      data: {
        AssesorId: Id,
        heading: this.heading
      }
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
       this.ConfirmDialog();
        this.GetBrokerGrid();
     }
    });
  }
  applyFilterAssessor() {
    this.filterValue = this.FilterAssessor.get('AssessorName').value;
    this.AssessorData.filter = this.filterValue.trim().toLowerCase();

    if (this.AssessorData.paginator) {
      this.AssessorData.paginator.firstPage();
    }
  }

  DeleteBroker(Id: number): void {
    if(Id<9)
    {
      this.dialog.open(SuggestionComponent, { width: '450px', height: '180px',data:{ dispalymessage: 'Record cannot be deleted due to integrity references'} });
    }
    else{
    const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {
        this._masterService.deleteAssessor(Id)
          .subscribe((response: any) => {
            this.ConfirmDialog();
            this.GetBrokerGrid();
          }, (error: any) => {
            if (error == null) {
              this.errorText='Internal Server error';
            }
            else {
              this.errorText=error;
            }
          });
      }
    });
  }
  }
  ClearBroker(): void {
    this.FilterAssessor.reset();
    this.GetBrokerGrid();
  }
  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }

}