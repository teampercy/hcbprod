import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatTableDataSource } from '@angular/material';
import { MasterService } from '../master.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AddEditHealthTrustComponent } from './add-edit-health-trust/add-edit-health-trust.component';
import { SuggestionComponent } from 'app/main/Common/suggestion/suggestion.component';
import { DeleteDialogComponent } from 'app/main/Common/delete-dialog/delete-dialog.component';
import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-health-trust',
  templateUrl: './health-trust.component.html',
  styleUrls: ['./health-trust.component.scss']
})
export class HealthTrustComponent implements OnInit {

  working: boolean;
  errorText: string;
  dialogRef: any;
  healthTrust: any;
  FilterHealthTrust: FormGroup;
  filterValue: string;
  UserName:string;
  displayColumns = ['Name', 'Id', 'delete'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(
    private _masterService: MasterService,
    private _formbuilder: FormBuilder,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
     this.FilterHealthTrust = this._formbuilder.group({
      NameHealthTrust: ['']
     });
     this.UserName = localStorage.getItem('UserName');
    this.GetHealthTrustGrid();
  }

  GetHealthTrustGrid(): void {
    this._masterService.GetMasterTypelistValues(28)
      .subscribe((response: any) => {
        this.healthTrust = new MatTableDataSource<any>(response['result']);
        this.healthTrust.sortingDataAccessor = (data: any, sortHeaderId: string) => {
          if (!data[sortHeaderId]) {
            return this.sort.direction === "asc" ? '3' : '1';
          }
          return '2' + data[sortHeaderId].toLocaleLowerCase();
        }
   
        this.healthTrust.paginator = this.paginator;
      }, (error: any) => {
        this.errorText = error;
      });
  }

  AddHealthTrust(): void {
     this.dialogRef = this.dialog.open(AddEditHealthTrustComponent, {
       disableClose: true,
       width: '400px',
       data: {
        HealthTrustId: 0
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
         this.ConfirmDialog();
         this.GetHealthTrustGrid();
       }
     });
   }

   EditHealthTrust(Id: number): void {
     this.dialogRef = this.dialog.open(AddEditHealthTrustComponent, {
       disableClose: true,
       width: '400px',
       data: {
        HealthTrustId: Id
       }
     });

     this.dialogRef.afterClosed().subscribe(result => {
       if (result) {
        this.ConfirmDialog();
         this.GetHealthTrustGrid();
      }
     });
   }

   DeleteHealthTrust(Id: number): void {
    this._masterService.MasterListdeleteOrNot(Id)
    .subscribe((response: any) => {
 
      if (response['result'][0].countId !=0)
       {
        this.dialog.open(SuggestionComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Record cannot be deleted due to integrity references'}});
      } 
      else {
        const dialogRef = this.dialog.open(DeleteDialogComponent, { width: '450px', height: '180px' ,data:{ dispalymessage: 'Are you sure you want to delete this Item?'} });
        dialogRef.afterClosed().subscribe(result => {
          if (result == 'Confirm') {
            this._masterService.deleteMasterListValueById(Id,'MasterList',28,this.UserName)
              .subscribe((response: any) => {
                this.ConfirmDialog();
                this.GetHealthTrustGrid();
              }, (error: any) => {
                if (error == null) {
                  this.errorText = 'Internal Server error';
                }
                else {
                  this.errorText = error;
                }
              });
          }
        });
      }
    })
  }

  applyFilterHealthTrust() {
    this.filterValue = this.FilterHealthTrust.get('NameHealthTrust').value;
    this.healthTrust.filter = this.filterValue.trim().toLowerCase();

    if (this.healthTrust.paginator) {
      this.healthTrust.paginator.firstPage();
    }
  }

  ClearHealthTrust(): void {
    this.FilterHealthTrust.reset();
    this.GetHealthTrustGrid();
  }

  ConfirmDialog() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, { width: '300px' });
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Confirm') {

      }
    });
  }

}
