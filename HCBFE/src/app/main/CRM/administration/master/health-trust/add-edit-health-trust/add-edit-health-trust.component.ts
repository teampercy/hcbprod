import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MasterService } from '../../master.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SaveMasterList } from '../../master.model';

@Component({
  selector: 'app-add-edit-health-trust',
  templateUrl: './add-edit-health-trust.component.html',
  styleUrls: ['./add-edit-health-trust.component.scss']
})
export class AddEditHealthTrustComponent implements OnInit {

  Count:number;
  name:string;
  NameTaken:boolean = false;
  errorText: string;
  working: boolean;
  TypeId:number;
  AddEditHealthTrust: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _master: MasterService,
    private _route: ActivatedRoute,
    public dialogRef: MatDialogRef<AddEditHealthTrustComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.HealthTrustId > 0) {
      this.AddEditHealthTrust = this._formBuilder.group({
        Id: [this.data.HealthTrustId],
        TypeId: [28],
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        RelatedValue:[null]
      })
      this.SetValues();
    } else {
      this.AddEditHealthTrust = this._formBuilder.group({
        Name: ['', Validators.required, [Validators.composeAsync([this.createvalidators(this._master)])]],
        TypeId: [28],
        RelatedValue:[null]
      })
    }
    this.TypeId=this.AddEditHealthTrust.value.TypeId;
  }

  createvalidators(_master: MasterService) {
    return control => new Promise((resolve: any) => {
  
      if (this.name == control.value || this.name == "") {
        resolve(null);
      }
      else{
        this._master.CheckMasterList(control.value,this.TypeId)
          .subscribe((response) => {
          //  console.log(response);
            this.Count = response['result'].masterName[0].resultCount;
          //  console.log(this.Count);
            if (this.Count != 0) {
               resolve({ NameTaken: true });
  
            } else {
              resolve(null);
            }
          }, (err) => {
            if (err !== "404 - Not Found") {
              resolve({ NameTaken: true });
            } else {
              resolve(null);
            }
          })
      }
    })

  }

  SetValues(): void {
    this._master.GetMasterListValueById(this.data.HealthTrustId)
      .subscribe((response: any) => {
        this.name= response['result'][0].name;
        this.AddEditHealthTrust.patchValue({ Name: response['result'][0].name });
      }, (error: any) => {
        this.data.Id = undefined;
        this.errorText = error;
      });
  }

  SaveHealthTrust(): void {
    if (this.AddEditHealthTrust.invalid)
      return;

    this.working = true;
    if (this.data.HealthTrustId) {
      const updateHealthTrust: SaveMasterList = Object.assign({}, this.AddEditHealthTrust.value);

      this._master.UpdateMasterListValue(updateHealthTrust)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    } else {
      this.working = true;
      this._master.SaveMasterListValue(this.AddEditHealthTrust.value)
        .subscribe((response: any) => {
          var Result = response['result'];
          this.dialogRef.close(true);
          this.working = false;
        }, (error: any) => {
          this.errorText = error;
          this.working = false;
        });
    }
  }

}
