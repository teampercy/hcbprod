export class GetCRMUserModel{

    UserID:number;
    EmailAddress:string;
    Password:string;
    UserType:string;
    FullName:string;
    TelephoneNumber:string;
    Address:string;
    AccountActive:number;
    IncoID:number;
    TermsAccepted:number;
    NominatedNurseID:number;
    LastLoginDate :number;
    FailedAttempt :number;
    PasswordExpiryDate :Date;
    City : string;
    IsOnline :number;
    UserTypeId :number;
    CanWrokAsCM: boolean;
}



export class AddUpdateUser
{
    UserID:number;
    EmailAddress:string;
    Password:string;
    UserType:string;
    FullName:string;
    TelephoneNumber:string;
    Address:string;
    AccountActive:number;
    FailedAttempt :number;
   // PasswordExpiryDate :Date;
    City : string;
    IsOnline :number;
    UserTypeId :number;
    CanWrokAsCM:boolean;
}

