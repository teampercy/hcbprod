import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';
import { MaterialModule } from 'app/main/Material.module';
import { AuthGuard } from 'app/custom/auth.guard';
//import { ConfirmDialogComponent } from 'app/main/Common/confirm-dialog/confirm-dialog.component';
import { ClientReportComponent } from './client-report.component';


const routes = [
    {
      path: 'administration/client-report',
      component: ClientReportComponent,
      canActivate: [AuthGuard]
    }
];

@NgModule({
    declarations: [
        ClientReportComponent     
    ],
    imports: [
      RouterModule.forChild(routes),
      TranslateModule,      
      FuseSharedModule,
      MaterialModule
    ]
  })
export class ClientReportModule{}