import { Injectable } from '@angular/core';
import { HttpClientsService } from 'app/service/http-client.service';
import { Observable } from 'rxjs';
import { CommonService } from 'app/service/common.service';
import { ClientReportGrid, GetClientReport, HCBClientReport } from './client-report.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { AuthenticationService } from 'app/service/authentication.service';
import { map } from 'rxjs/internal/operators/map';
import { Response } from 'selenium-webdriver/http';
import * as _moment from 'moment';
import * as FileSaver from 'file-saver';
import { saveAs } from 'file-saver';
import * as XLSX from 'xlsx';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { ArrayType } from '@angular/compiler/src/output/output_ast';


const moment = _moment;
import { Moment } from 'moment';
import { style } from '@angular/animations';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class ClientReportService {
  list = new HCBClientReport();
  d: any;
  str:string;
  constructor(

    private _commonService: CommonService,
    //private _exceptionHandler: ExceptionService,
    private _http: HttpClientsService,
    private _authentication: AuthenticationService,
    private http: HttpClient) { this.list = JSON.parse(sessionStorage.getItem('Report')) || new HCBClientReport(); }

  GetCRMClientReportList(model: ClientReportGrid): Observable<GetClientReport[]> {
  
    return this._http.postAPI("ClientReport/GetCRMClientReportList", model);
  }

  GetddlServices() {
    return this._http.getAPI("ClientReport/GetddlServiceList");
  }
  GetRelationshipOwner() {
 
    return this._http.getAPI("CRMClientRecord/GetRelationshipOwner");
  }




  public exportAsExcelFile(model: any) {

    for (var i = 0; i < model.length; i++) {
      delete model[i].clientID;
      delete model[i].clientTypeId;
    }
    for (var i = 0; i < model.length; i++) {
      model[i].createdOn = moment({ year: this._commonService.GetYearFromDateString(model[i].createdOn), month: this._commonService.GetMonthFromDateString(model[i].createdOn), date: this._commonService.GetDateFromDateString(model[i].createdOn) }).format("DD/MM/YYYY")

    }

    for (var i = 0; i < model.length; i++) {
      model[i]['Company Name'] = model[i].companyName;
      model[i]['Client Added Date'] = model[i].createdOn;
      model[i]['Client Type'] = model[i].ClientType;
      model[i]['RelationshipOwner'] = model[i].relationshipOwner;
      model[i]['Principal Contact'] = model[i].principalContact;
      model[i]['Principal Contact Email'] = model[i].principalContactEmail;
      model[i]['Contact Names'] = model[i].contactName;
      model[i]['Contact Email Address'] = model[i].emailAddress;
      delete model[i].companyName;
      delete model[i].createdOn;
      delete model[i].ClientType;
      delete model[i].relationshipOwner;
      delete model[i].principalContact;
      delete model[i].principalContactEmail;
      delete model[i].contactName;
      delete model[i].emailAddress;
    }

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(model);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer);

  }


  private saveAsExcelFile(buffer: any): void {
  
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, 'client_Report' + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  generatePdf(model: any[]) {

    for (var i = 0; i < model.length; i++) {
      delete model[i].clientID;
      delete model[i].clientTypeId;
    }
    for (var i = 0; i < model.length; i++) {
      model[i].createdOn = moment({ year: this._commonService.GetYearFromDateString(model[i].createdOn), month: this._commonService.GetMonthFromDateString(model[i].createdOn), date: this._commonService.GetDateFromDateString(model[i].createdOn) }).format("DD/MM/YYYY")

    }

    const documentDefinition = this.getDocumentDefinition(model);
 
    documentDefinition.styles.tablebody
    pdfMake.createPdf(documentDefinition).download("client_Report.pdf");

  }
  getDocumentDefinition(model: any[]) {
   

    var docDefinition = {
      content: [
      
        { text: 'Client Report', style: 'header', alignment: 'center' },

        this.table(model, ['companyName', 'createdOn', 'ClientType', 'relationshipOwner', 'principalContact', 'principalContactEmail','emailAddress'])
      ],
  
      pageSize: 'A3',
      pageMargins: [10,10,10,10],

      styles: {
        header: {
          fontSize: 10,
          bold: true,
          
        },
        tableHeader: {
          fontSize:10,
          bold: true,
        
        },
      tablebody:{
        fontSize:10
      },
      
      },
    }
    return docDefinition;
  }

  table(data, columns) {
 
    return {
      table: {
        headerRows: 1,
        widths: [ 75,75,65,75,70,85,'*'] ,
       // widths: [ 70,65,60,60,65,92,100] ,  A4
       // 74,74,74,74,74,74]
        fontSize:10,
      
        body: this.buildTableBody(data, columns),
        
      }
    };
  }

  buildTableBody(data, columns) {
   
    var body = [];
    body.push([{ text: 'Company Name', style: 'tableHeader'}, { text: 'Client Added Date', style: 'tableHeader' }, { text: 'Client Type', style: 'tableHeader' }, { text: 'Relationship Owner', style: 'tableHeader' }, { text: 'Principal Contact', style: 'tableHeader' }, { text: 'Pricipal Contact Email', style: 'tableHeader' }, { text: 'Contact Email Address', style: 'tableHeader' }]);
    data.forEach(function (row) {
      var dataRow = [];
      columns.forEach(function (column) { 
         dataRow.push(row[column]);
      })
  
      body.push(dataRow);    

    });

    return body;
  }

}

