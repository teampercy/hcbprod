import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FuseConfigService } from '@fuse/services/config.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginError: string;
  working: boolean;
  Text:string;
  displayImage: boolean;

  constructor(

    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _auth: AuthService,
    private _fuseNavigationService: FuseNavigationService
  ) {
    // Configure the layout
    var queryStringValue = window.location.href.split('#')[0];
  if (queryStringValue.toLowerCase().indexOf("/crm") > -1) {     
     localStorage.setItem('Site', "2");
    }
    else {
      localStorage.setItem('Site', "1");
    }
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit(): void {
    
    this.loginError = undefined;
    this.working = false;
    this.loginForm = this._formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
    var queryStringValue = window.location.href.split('#')[0];
    if ((queryStringValue.toLowerCase().indexOf("/crm") > -1)) {
      //  this._router.navigate(['/crm-dashboard']);
      // localStorage.setItem('Site', "2");
     // this.displayImage = true;
     this.Text='Client';
    }
    else {
      // this.navigation = navigation.map(x => Object.assign({}, x));
      //    localStorage.setItem('Site', "1");
     // this.displayImage = false;
     this.Text ='Operational';
    }
  }

  doLogin(loginForm): void {
 
    this.loginError = undefined;
    this.working = true;


    this._auth.login(this.loginForm.value.username, this.loginForm.value.password)
      .subscribe((Result: any) => {
     
        var queryStringValue = window.location.href.split('#')[0];
        if ((queryStringValue.toLowerCase().indexOf("/crm") > -1) && (Result.userTypeId == 1 ||
          Result.userTypeId ==2) ) {
          
          this._router.navigate(['/crm-dashboard']);

        }
        else if ((queryStringValue.toLowerCase().indexOf("/crm") > -1) && (Result.userTypeId != 1)) {
       
          this._router.navigate(['/login']);
          this.loginError = "Invalid User";
          this.working = false;

        }
        else if (Result.userTypeId == 4) {
          this._router.navigate(['/login']);
          this.loginError = "Invalid User";
          this.working = false;
        }
        else {
       
        //  this._router.navigate(['/login']);  //     /dashboard
          this._router.navigate(['/dashboard']);
        }

      }, (error: any) => {
        // if(error=='OK')
        // {
          this.loginError= "Incorrect Username Or Password";
        // }else{
        // this.loginError = error;
        // }
        this.working = false;
      });

  }

}