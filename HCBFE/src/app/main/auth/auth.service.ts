import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'app/model/user';
import { map } from 'rxjs/operators';
import { HttpClientsService } from 'app/service/http-client.service';
import { AuthenticationService } from 'app/service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  constructor(
    private http: HttpClientsService,
    private _authentication: AuthenticationService
    ) {}


  login(username: string, password: string) {
 debugger
    return this.http.postAPI('Login', { username, password })
      .pipe(map(result => {
      
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        //  localStorage.setItem('currentUser', JSON.stringify(user));
        this._authentication.SetToken(result.user.token);
        localStorage.setItem('UserType', result.user.userType);
        localStorage.setItem('UserTypeId', result.user.userTypeId);
        localStorage.setItem('UserId', result.user.userId);
        localStorage.setItem('UserName',result.user.name);
        localStorage.setItem('ClientId',result.user.clientId);

        localStorage.setItem('Password',result.user.password);
        localStorage.setItem('Email',result.user.username);
        
        localStorage.setItem('CanWorkAsCM', result.user.canWorkAsCM);
       

        localStorage.setItem('isLoggedin', "1");
        sessionStorage.setItem('isLoggedin', "1");
        // localStorage.setItem('currentUser', JSON.stringify(result.user));
        // this.currentUserSubject.next(result.user);
        return result.user;
      }));
  }


}
