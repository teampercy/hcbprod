import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { Router, ActivatedRoute, Params, UrlSegment} from '@angular/router';

@Component({
  selector: 'app-alert-message',
  templateUrl: './alert-message.component.html',
  styleUrls: ['./alert-message.component.scss'],

   animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})

export class AlertMessageComponent implements OnInit {

FistName:string;
LastName:string;
ReferenceNo:string;

constructor(public dialogRef: MatDialogRef<AlertMessageComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

    this.FistName=this.data.Firstname;
    this.LastName = this.data.Lastname;
    this.ReferenceNo = this.data.HCBReferenceNo;

  }
  // onCloseConfirm(){
  //   this.dialogRef.close('Confirm');
  // }

  // onCloseClose(){
  //   this.dialogRef.close('Close');
  // }

}
