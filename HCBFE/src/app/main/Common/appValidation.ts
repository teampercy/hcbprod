
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
export class AppCustomDirective extends Validators {

  static fromDateValidator(dateValue: AbstractControl) {
   
    if (dateValue.status == "VALID" && dateValue.value == null && dateValue.errors!=null) {
      return { 'dateVaidator': true };
    }
    return null;
  }

  static fromDateValidator1(dateValue: AbstractControl)
  {
   
   var today = new Date();
    //console.log(dateValue);
    //console.log(today.toDateString);
    if ( dateValue.value <today) {
      return { 'dateVaidator1': true };
    }
    return null;
  }


  static ToDateValidator(todValue: FormControl) {
    const date = todValue.value;
    if (date === null || date === '') {
      return { requiredToDate: true };
    }
    return { requiredToDate: false };
  }

  // Not working
  static timeValidator(formGroupValues: FormGroup): any {
    //console.log(formGroupValues);
    const FromDate = formGroupValues.get('FromDate').value;
    const ToDate = formGroupValues.get('ToDate').value;
    const FromTime = formGroupValues.get('FromTime').value;
    const ToTime = formGroupValues.get('ToTime').value;

    if (FromDate.toString() === ToDate.toString()) {
      let fromTime = [];
      let toTime = [];
      fromTime = FromTime.split(':');
      toTime = ToTime.split(':');
      if (parseInt(fromTime[0]) > parseInt(toTime[0])) {
        alert("condition satisfied not return any error message");
        return { InValidToTime: true };
      }
      else if (
        parseInt(fromTime[0]) === parseInt(toTime[0]) &&
        parseInt(fromTime[1]) > parseInt(toTime[1])
      ) {
        alert("condition satisfied not return any error message")
        return { InValidToTime: true };
      }
    }
  }
}