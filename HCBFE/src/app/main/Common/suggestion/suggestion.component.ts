import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { trigger, style, state, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-suggestion',
  templateUrl: './suggestion.component.html',
  styleUrls: ['./suggestion.component.scss'],

  animations: [
    fuseAnimations,
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  encapsulation: ViewEncapsulation.None
})
export class SuggestionComponent implements OnInit {

  messagedisplay:string;

  constructor(public dialogRef: MatDialogRef<SuggestionComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { }
  ngOnInit() {
    this.messagedisplay=this.data.dispalymessage;
  }

  onCloseConfirm(){
    this.dialogRef.close('Confirm');
  }

  onCloseClose(){
    this.dialogRef.close('Close');
  }
}