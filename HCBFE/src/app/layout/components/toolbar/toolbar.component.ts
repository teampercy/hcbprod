import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import * as _ from 'lodash';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { navigation, navigationCRM } from 'app/navigation/navigation';
import { Router } from '@angular/router';
import { AuthService } from 'app/main/auth/auth.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
//import { navigation, navigationCRM } from 'app/navigation/navigation';

@Component({
    selector: 'toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class ToolbarComponent implements OnInit, OnDestroy {
    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    languages: any;
    navigation: any;
    selectedLanguage: any;
    userStatusOptions: any[];
    user: number;
    userName: string;
    Title: string;
    SubTitle: string;
    MenuName: string;
    displayOrNot: boolean;
    displayOrNotimportcases: boolean;
    ShowToolbar:boolean;
    ShowOPSToolbar:boolean;
    UserFirstName:string;

    // Private
    private _unsubscribeAll: Subject<any>;
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {TranslateService} _translateService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseSidebarService: FuseSidebarService,
        private _translateService: TranslateService,
        private _auth: AuthService,
        private _router: Router,
        private _fuseNavigationService: FuseNavigationService
    ) {
        // Set the defaults
        this.userStatusOptions = [
            {
                title: 'Online',
                icon: 'icon-checkbox-marked-circle',
                color: '#4CAF50'
            },
            {
                title: 'Away',
                icon: 'icon-clock',
                color: '#FFC107'
            },
            {
                title: 'Do not Disturb',
                icon: 'icon-minus-circle',
                color: '#F44336'
            },
            {
                title: 'Invisible',
                icon: 'icon-checkbox-blank-circle-outline',
                color: '#BDBDBD'
            },
            {
                title: 'Offline',
                icon: 'icon-checkbox-blank-circle-outline',
                color: '#616161'
            }
        ];

        this.languages = [
            {
                id: 'en',
                title: 'English',
                flag: 'us'
            },
            {
                id: 'tr',
                title: 'Turkish',
                flag: 'tr'
            }
        ];

        this.navigation = navigation;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
      
        ////New changes ///////
        // var queryStringValue = window.location.href.split('#')[0];



        if (parseInt(localStorage.getItem('UserTypeId')) == 1 || parseInt(localStorage.getItem('UserTypeId')) == 2) {
            this._fuseNavigationService.updateNavigationItem('manage-list', {
                hidden: false
              });
               this._fuseNavigationService.updateNavigationItem('import-Cases', {
                hidden: false
              });
            this.displayOrNot = true;
        }
        else {
            this._fuseNavigationService.updateNavigationItem('manage-list', {
                hidden: true
              });
              this._fuseNavigationService.updateNavigationItem('import-Cases', {
                hidden: true
              });
           
            this.displayOrNot = false;
        }


        //End///

        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((settings) => {
                this.horizontalNavbar = settings.layout.navbar.position === 'top';
                this.rightNavbar = settings.layout.navbar.position === 'right';
                this.hiddenNavbar = settings.layout.navbar.hidden === true;
            });

        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, { id: this._translateService.currentLang });
        this.user = parseInt(localStorage.getItem('UserTypeId'));
        this.userName = localStorage.getItem('UserName');
this.UserFirstName =this.userName.split(' ')[0];
        this.Title = "Operational Records and Client Administration System"
        var queryStringValue = window.location.href.split('#')[0];
        var queryStringValue1 = window.location.href.split('#')[1];
        // console.log(queryStringValue1);
        if ((queryStringValue.toLowerCase().indexOf("/crm") > -1) && ((this.user == 1) || (this.user == 2))) {
            this.SubTitle = "Client Relationship Management"
           // this.MenuName = "HCB";
           this.MenuName = "Ops Module";
           this.ShowToolbar=true;
           this.ShowOPSToolbar=false;
        }
        else if ((queryStringValue1.toLowerCase().indexOf("/administration") > 1) &&((this.user == 1) ||(this.user == 2))) {
            this.SubTitle = "User Managment"
        }
        else {
          //  this.SubTitle = "Case Management System"
            this.ShowToolbar=false;
            this.ShowOPSToolbar=true;
           // this.MenuName = "ORCA";
           this.MenuName = "CRM";

        }
        // console.log(queryStringValue1.toLowerCase().indexOf("/administration") > -1);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    /**
     * Search
     *
     * @param value
     */
    search(value): void {
        // Do your search here...
        console.log(value);
    }

    /**
     * Set the language
     *
     * @param lang
     */
    setLanguage(lang): void {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;

        // Use the selected language for translations
        this._translateService.use(lang.id);
    }

    Logout(): void {

        // localStorage.setItem("Token", null);
        localStorage.removeItem("UserType");
        // localStorage.setItem("UserType", null);
        localStorage.removeItem("UserId");
        //  localStorage.setItem("UserId", null);
        localStorage.removeItem("UserName");
        //  localStorage.setItem("UserName", null);
        localStorage.removeItem("Token");
        //  localStorage.setItem("UserTypeId", null);
        localStorage.removeItem("UserTypeId");
        localStorage.removeItem("CanWorkAsCM");
        //  localStorage.setItem("ClientId", null);
        localStorage.removeItem("ClientId");
        localStorage.removeItem("isLoggedin");
        sessionStorage.removeItem("isLoggedin");
        localStorage.removeItem("Site");
        localStorage.clear();
        // return this._auth.logout();
        // console.log(localStorage.getItem('site'));
        console.log("site is null");
        this._router.navigate(['/login']);
    }

    switchPage() {
       // alert("Page Change");

         if (this.MenuName == "CRM") {
        //     debugger;
        //     alert("Page Change to HCB");
        //     window.location.href = "http://dev.rhealtech.com/HCB/#/dashboard/"
        //     //window.location.href = "/HCB/CRM/#/crm-dashboard/"
        // }
       // if (window.location.href == "http://dev.rhealtech.com/HCB/#/dashboard") {
          //  window.location.href = "http://dev.rhealtech.com/HCB/CRM/#/crm-dashboard/"  //for dev
          // window.location.href = "https://azuredev.rhealtech.com/HCB/CRM/#/crm-dashboard/" //for azure        
            window.location.href = "https://orca.hcbgroup.co.uk/CRM/#/crm-dashboard/"
             
        }

        else { 
           // alert("Page Change to ORCA");
            //window.location.href = "/HCB/CRM/#/crm-dashboard/"
           // window.location.href = "http://dev.rhealtech.com/HCB/#/dashboard"  //for dev
         //  window.location.href = "https://azuredev.rhealtech.com/HCB/#/dashboard"  //for  azure     
         window.location.href = "https://orca.hcbgroup.co.uk/#/dashboard"

        }
    }
}
