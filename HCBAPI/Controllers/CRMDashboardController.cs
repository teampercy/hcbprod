using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
// using System.Web.Http;
using System.Security.Claims;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HCBNewAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class CRMDashboardController : ControllerBase
    {
        CRMDashboard objCRMDashboard;
        IConfiguration _configuration;

        private int GetUserId()
        {
            return int.Parse(this.User.Claims.Where(i => i.Type == "UserId").Select(i => i.Value).FirstOrDefault());
        }

        public CRMDashboardController(IConfiguration configuration, CRMDashboard CRMDashboardlayer)
        {
            _configuration = configuration;
            objCRMDashboard = CRMDashboardlayer;
        }

          [HttpGet]

        public IActionResult GetCRMMostActiveClients(string startdate,string enddate,int userId,string usertype ,int clientTypeId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMDashboard.GetCRMMostActiveClients(startdate,enddate,userId,usertype, clientTypeId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetActiveClientsType()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMDashboard.GetActiveClientsType() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardRegionPercentage()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMDashboard.GetDashboardRegionPercentage() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetCasesPerClient(bool IsOpen, int ClientId, string startdate, string enddate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMDashboard.GetCasesPerClient(IsOpen, ClientId, startdate, enddate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetCasesPerCaseMgr(bool IsOpen, int CaseMgrId, string startdate, string enddate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMDashboard.GetCasesPerCaseMgr(IsOpen, CaseMgrId, startdate, enddate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetTotalClients()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMDashboard.GetTotalClients() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetActiveInActiveClient(int IsActive)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMDashboard.GetActiveInActiveClient(IsActive) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardClientsPerServices(int ServiceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMDashboard.GetDashboardClientsPerServices(ServiceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardNoActivityReport(int ClientId, int NoActivityInterval,int UserId,string Usertype)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMDashboard.GetDashboardNoActivityReport(ClientId, NoActivityInterval,UserId,Usertype) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]
        public IActionResult GetClientListForService(int ServiceTypeId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMDashboard.GetClientListForService(ServiceTypeId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    }
}