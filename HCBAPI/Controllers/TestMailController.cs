using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Linq;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
//using System.Web.Http;
using System.Security.Claims;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HCBNewAPI.Controllers
{
   [ApiController]
   [Route("api/[controller]/[action]")]
    public class TestMailController : ControllerBase
    {
        TestMail objTestMail;
    
        IConfiguration _configuration;
         private int GetUserId()
         {
             return int.Parse(this.User.Claims.Where(i => i.Type == "UserId").Select(i => i.Value).FirstOrDefault());
         }

        public TestMailController(IConfiguration configuration, TestMail testmailRecordayer)
        {
            _configuration = configuration;
            objTestMail = testmailRecordayer;

        }
         [HttpGet]
        public IActionResult TestMail(string SMTPHost,string Username ,string Password ,string FromEmail,string SenderEmail,string Port)  
        {
            try
              {
                return Ok(new { result = objTestMail.testmail(SMTPHost,Username,Password,FromEmail,SenderEmail,Port) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    }
}