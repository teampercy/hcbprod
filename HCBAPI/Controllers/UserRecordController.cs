using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Linq;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
//using System.Web.Http;
using System.Security.Claims;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HCBNewAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class UserRecordController : ControllerBase
    {
        UserRecord objCRMUserRecord;

        IConfiguration _configuration;
        private int GetUserId()
        {
            return int.Parse(this.User.Claims.Where(i => i.Type == "UserId").Select(i => i.Value).FirstOrDefault());
        }

        public UserRecordController(IConfiguration configuration, UserRecord CRMUserRecordayer)
        {
            _configuration = configuration;
            objCRMUserRecord = CRMUserRecordayer;

        }

        [HttpGet]
        public ActionResult<string> GetCRMUserName(int id)
        {
            return "user " + id.ToString();
        }

        [HttpPost]
        public IActionResult GetCRMUserList([FromBody] UserRecordModel model)
        {

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMUserRecord.GetUserRecordList(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetUserRecordById(int UserID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            //HttpResponseMessage result = null;
            try
            {
                // int UserId=GetUserId();
                //    UserRecordModel obj = objCRMUserRecord.GetUserRecordById(UserID) ;

                return Ok(new { result = objCRMUserRecord.GetUserRecordById(UserID) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]
        public IActionResult GetCRMUserType()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMUserRecord.GetCRMUserType() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateCRMUserRecord([FromBody] AddUpdatecrmUser model) //[FromBody]
        {
            model.Password = Encryption.Encrypt(model.Password);
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                 int UserId=GetUserId();
                 model.ModifiedBy= UserId;
                return Ok(new { result = objCRMUserRecord.UpdateCRMUserRecord(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult AddcrmUser([FromBody] AddUpdatecrmUser model) //[FromBody]
        {
            int UserId = GetUserId();
            model.Password = Encryption.Encrypt(model.Password);
            model.CreatedBy = UserId;
      HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMUserRecord.AddCRNUser(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        [HttpGet]
        public IActionResult CheckCRMUseremail(string emailAddress, int UserId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMUserRecord.CheckCRMUserEmail(emailAddress, UserId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

       [HttpGet]
        public IActionResult GetlServiceProviderlist()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMUserRecord.GetlServiceProviderlist() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

         [HttpGet]
        public IActionResult GetCaseRecordsForCM(int UserId, int usertypeId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMUserRecord.GetCaseRecordsForUser(UserId,usertypeId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}