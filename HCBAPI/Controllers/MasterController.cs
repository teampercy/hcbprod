using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
// using System.Web.Http;
using System.Security.Claims;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HCBNewAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class MasterController : ControllerBase
    {
        Master objMaster;
        IConfiguration _configuration;

        private int GetUserId()
        {
            return int.Parse(this.User.Claims.Where(i => i.Type == "UserId").Select(i => i.Value).FirstOrDefault());
        }

        public MasterController(IConfiguration configuration, Master Masterlayer)
        {
            _configuration = configuration;
            objMaster = Masterlayer;
        }


        [HttpGet]

        public IActionResult GetddlTypelistValues(int TypeId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.GetddlTypelistValues(TypeId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetMasterListValueById(int Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.GetMasterListValueById(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult SaveMasterListValue([FromBody]MasterModel model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.SaveMasterListValue(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult UpdateMasterListValue([FromBody]MasterModel model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.UpdateMasterListValue(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult deleteMasterListValueById(int Id,string Table,int TypeID,string UserName)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int UserId=GetUserId();
                return Ok(new { result = objMaster.deleteMasterListValueById(Id,UserId,Table,TypeID,UserName) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult deleteRelationshipOwnerById(int Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                 int UserId=GetUserId();
                return Ok(new { result = objMaster.deleteRelationshipOwnerById(Id, UserId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

                [HttpGet]

        public IActionResult deleteBrokerById(int Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                 int UserId=GetUserId();
                return Ok(new { result = objMaster.deleteBrokerById(Id, UserId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        // [HttpGet]

        // public IActionResult GetAssessorsDetails()
        // {
        //     HttpResponseMessage response = new HttpResponseMessage();
        //     try
        //     {
        //         return Ok(new { result = objMaster.GetAssessorsDetails() });
        //     }
        //     catch (Exception ex)
        //     {
        //         return StatusCode(500, ex);
        //     }
        // }

         [HttpGet]

        public IActionResult GetAssessorList()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.GetAssessorList() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        [HttpGet]
        public IActionResult GetAssessorDeatailById(int Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.GetAssessorDeatailById(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateAssessorDeatail([FromBody]UpdateAssesor model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.UpdateAssessorDeatail(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult AddAssessorDeatail([FromBody]UpdateAssesor model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.AddAssessorDeatail(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult deleteAssessor(int Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.deleteAssessor(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]
        public IActionResult MasterListdeleteOrNot(int Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.MasterListdeleteOrNot(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]
        public IActionResult CheckMasterList(string Name,int TypeId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objMaster.CheckMasterlist(Name,TypeId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

          [HttpGet]
        public IActionResult CheckAssessorExistOrNot(string Name)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objMaster.CheckAssessorExistOrNot(Name) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
public IActionResult GetServiceProviderCompany()
{
    HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objMaster.GetServiceProviderCompany() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            } 
}
public IActionResult AddSPCompany(AddEditSpCompany model)
{
     HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objMaster.AddSPCompany(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            } 
}
public IActionResult UpdateSPCompany(AddEditSpCompany model)
{
     HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objMaster.UpdateSPCompany(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            } 
}
 [HttpGet]
        public IActionResult GetSPCompanyDetailById(int CompanyId)
        {
            
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.GetSPCompanyDetailById(CompanyId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

public IActionResult SPCompanyDeleteOrNot(int Id)
{
      HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.SPCompanyDeleteOrNot(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
}

public IActionResult DeleteSPcompany(int Id)
{
      HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objMaster.DeleteSPcompany(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
}
 [HttpGet]
        public IActionResult CheckSPCompanyExist(string Name,int CompanyId)
 {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objMaster.CheckSPCompanyExist(Name,CompanyId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    }
}