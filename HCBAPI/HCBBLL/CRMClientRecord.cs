using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HCBNewAPI.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Net.Mail;

namespace HCBNewAPI.HCBBLL
{
    public class CRMClientRecord
    {
        IConfiguration _configuration;
        public string connectionstring;
        private Database db;
        int Noteid;


        public CRMClientRecord(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionstring = _configuration["ConnectionStrings:DefaultConnection"];
        }

        public DataSet GetClientRecordList(GetCRMClientRecordGrid model)
        {
            DataSet ds = new DataSet();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCRMClients", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@pageIndex", model.pageIndex);
                    cmd.Parameters.AddWithValue("@pageSize", model.pageSize);
                    cmd.Parameters.AddWithValue("@sort", model.sort);
                    cmd.Parameters.AddWithValue("@direction", model.direction);
                    cmd.Parameters.AddWithValue("@CompanyName", model.CompanyName);
                    cmd.Parameters.AddWithValue("@ClientTypeId", model.ClientTypeId);
                    cmd.Parameters.AddWithValue("@CreatedOn", model.clientAddedDate == "" ? null : model.clientAddedDate);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // return ds.Tables[0];
            ds.Tables[0].TableName = "CRMClientRecordGrid";
            ds.Tables[1].TableName = "CRMClientRecordLength";
            return (ds);
        }


 public DataTable GetClientProcessInfoById(int ClientId)
        {
             DataSet ds = new DataSet();
             DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientProcessInfo", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientID", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        // DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        ds.Tables[0].Rows[0]["EncryptedPassword"] = Encryption.Decrypt(Convert.ToString(dt.Rows[0]["EncryptedPassword"]));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
         public DataTable GetEmailAddress(int ClientId)
          {
             DataSet ds = new DataSet();
             DataTable dt = new DataTable();
            try
             {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetReportEmailList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetClientRecordById(int ClientId)
        {
            DataSet ds = new DataSet();
             DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCRMClient", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientID", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int SaveCrmClientRecord(SaveContactViewModel model)
        {
DataSet ds = new DataSet();
int result=0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertCRMClient", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@ClientId";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);

                    cmd.Parameters.AddWithValue("@ClientTypeId", model.saveCRMClientRecord.ClientTypeId);
                    cmd.Parameters.AddWithValue("@CompanyName", model.saveCRMClientRecord.CompanyName);
                    cmd.Parameters.AddWithValue("@HeadOfficeAddress", model.saveCRMClientRecord.HeadOfficeAddress);
                    cmd.Parameters.AddWithValue("@RegionId", model.saveCRMClientRecord.RegionId);
                    cmd.Parameters.AddWithValue("@Town", model.saveCRMClientRecord.Town);
                    cmd.Parameters.AddWithValue("@Postcode", model.saveCRMClientRecord.Postcode);
                    cmd.Parameters.AddWithValue("@LandlineNo", model.saveCRMClientRecord.LandlineNo);
                    cmd.Parameters.AddWithValue("@MobileNo", model.saveCRMClientRecord.MobileNo);
                    cmd.Parameters.AddWithValue("@PrincipalContactName", model.saveCRMClientRecord.PrincipalContactName);
                    cmd.Parameters.AddWithValue("@PrincipalContactNo", model.saveCRMClientRecord.PrincipalContactNo);
                    cmd.Parameters.AddWithValue("@PrincipalContactEmail", model.saveCRMClientRecord.PrincipalContactEmail);
                    cmd.Parameters.AddWithValue("@BrokerNames", model.saveCRMClientRecord.BrokerId);
                    cmd.Parameters.AddWithValue("@PensionScheme", model.saveCRMClientRecord.PensionScheme);
                    cmd.Parameters.AddWithValue("@FinalSalary", model.saveCRMClientRecord.FinalSalary);
                    cmd.Parameters.AddWithValue("@IllHealthEarlyRetirement", model.saveCRMClientRecord.IllHealthEarlyRetirement);
                    cmd.Parameters.AddWithValue("@IPInsurerName", model.saveCRMClientRecord.IPInsurerName);
                    cmd.Parameters.AddWithValue("@DeferredPeriod", model.saveCRMClientRecord.DeferredPeriod);
                    cmd.Parameters.AddWithValue("@Duration", model.saveCRMClientRecord.Duration);
                    cmd.Parameters.AddWithValue("@RenewalDue", model.saveCRMClientRecord.RenewalDue);
                    cmd.Parameters.AddWithValue("@DefinitionOfOccupation", model.saveCRMClientRecord.DefinitionOfOccupation);
                    cmd.Parameters.AddWithValue("@MIInsurerAdminName", model.saveCRMClientRecord.MIInsurerAdminName);
                    cmd.Parameters.AddWithValue("@CPInsurerName", model.saveCRMClientRecord.CPInsurerName);
                    cmd.Parameters.AddWithValue("@EAPProviderName", model.saveCRMClientRecord.EAPProviderName);
                    cmd.Parameters.AddWithValue("@DaysPaidSickLeave", model.saveCRMClientRecord.DaysPaidSickLeave);
                    cmd.Parameters.AddWithValue("@OHPprovider", model.saveCRMClientRecord.OHPprovider);
                    cmd.Parameters.AddWithValue("@Recording", model.saveCRMClientRecord.Recording);
                    cmd.Parameters.AddWithValue("@RelationshipOwner", model.saveCRMClientRecord.RelationshipOwner);
                    cmd.Parameters.AddWithValue("@MeetingIntervals", model.saveCRMClientRecord.MeetingIntervals);
                    cmd.Parameters.AddWithValue("@FormalContractReqd", model.saveCRMClientRecord.FormalContractReqd);
                    cmd.Parameters.AddWithValue("@ContractSignedDate", string.IsNullOrEmpty(model.saveCRMClientRecord.ContractSignedDate) ? null : model.saveCRMClientRecord.ContractSignedDate);
                    cmd.Parameters.AddWithValue("@ContractExpiryDate", string.IsNullOrEmpty(model.saveCRMClientRecord.ContractExpiryDate) ? null : model.saveCRMClientRecord.ContractExpiryDate);
                   // cmd.Parameters.AddWithValue("@ContractSignedDate", model.saveCRMClientRecord.ContractSignedDate.ToString() == null || model.saveCRMClientRecord.ContractSignedDate.ToString() == " " ? null : model.saveCRMClientRecord.ContractSignedDate);
                   // cmd.Parameters.AddWithValue("@ContractExpiryDate", model.saveCRMClientRecord.ContractExpiryDate.ToString() == null || model.saveCRMClientRecord.ContractExpiryDate.ToString() == " " ? null : model.saveCRMClientRecord.ContractExpiryDate);
                    cmd.Parameters.AddWithValue("@ConsentToUseLogo", model.saveCRMClientRecord.ConsentToUseLogo);
                    cmd.Parameters.AddWithValue("@IsActive", model.saveCRMClientRecord.Status);
                    cmd.Parameters.AddWithValue("@CreatedBy", model.saveCRMClientRecord.CreatedBy);
                    cmd.Parameters.AddWithValue("@ClientServices", model.saveCRMClientRecord.ClientServices);

                    cmd.Parameters.AddWithValue("@RefSourceId", model.saveCRMClientRecord.RefSourceId);
                   ds= CheckCompanyName(model.saveCRMClientRecord.CompanyName,0);
                   int count = Convert.ToInt32(ds.Tables[0].Rows[0]["resultCount"]);
                
                   if(count==0)
                   {
                     result =Convert.ToInt32(cmd.ExecuteScalar());
                    if (result != null)
                    {
                        foreach (var item in model.saveReferralContact)
                        {
                            item.ClientId = result;
                            item.CreatedBy = model.saveCRMClientRecord.CreatedBy;
                            SaveReferralContact(item);
                        }
                        foreach (var accountItem in model.saveAccountContact)
                        {
                            accountItem.clientId = result;
                            accountItem.CreatedBy = model.saveCRMClientRecord.CreatedBy;
                            SaveAccountContact(accountItem);
                        }
                        foreach (var noteItem in model.saveNoteModel)
                        {
                            noteItem.ClientId = result;
                            noteItem.NoteCreatedByUserId = model.saveCRMClientRecord.CreatedBy;
                            SaveNote(noteItem);
                            if (model.saveCRMClientRecord.SendNotificationEmail == true)
                            {
                                SendNoteEmail(model.saveCRMClientRecord.CompanyName, model.saveCRMClientRecord.CreatedBy, model.saveCRMClientRecord.UserName, Noteid);
                            }
                        }
                        
                    }
                   
                    }else{

                    }
                  return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
             
        }

        public Boolean AddClientProcessInfo(SaveUpdateClientProcessInfoViewModel model)
        {
            try{
 using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertCRMClientProcessInfo", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientID", model.SaveUpdateClientInfo.ClientId);
                    cmd.Parameters.AddWithValue("@DateOfInformation", model.SaveUpdateClientInfo.DateOfInformation);
                    cmd.Parameters.AddWithValue("@AgreedSLAId", model.SaveUpdateClientInfo.AgreedSLAId);
                    cmd.Parameters.AddWithValue("@DefinedPersonalisedSLA", model.SaveUpdateClientInfo.DefinedSLA);
                    cmd.Parameters.AddWithValue("@ReportingFormatId", model.SaveUpdateClientInfo.ReportingFormat);
                    cmd.Parameters.AddWithValue("@EmailAddress", model.SaveUpdateClientInfo.EncryptedEmailAddress);
                    cmd.Parameters.AddWithValue("@IsTLSEnabled", model.SaveUpdateClientInfo.IsTLSEnabled);
                    cmd.Parameters.AddWithValue("@TLSDate", model.SaveUpdateClientInfo.TLSDate);
                    cmd.Parameters.AddWithValue("@EncryptedPassword", model.SaveUpdateClientInfo.EncryptedPassword);
                    cmd.Parameters.AddWithValue("@InvoiceFrequencyId ", model.SaveUpdateClientInfo.InvoiceFrequency);
                    cmd.Parameters.AddWithValue("@SpecificInstruction", model.SaveUpdateClientInfo.SpecificInstruction);
                    cmd.Parameters.AddWithValue("@ModifiedBy", model.SaveUpdateClientInfo.LastModifiedBy);
                    cmd.Parameters.AddWithValue("@IsOrederRequired", model.SaveUpdateClientInfo.IsPurchesOrderRequired);
                    cmd.Parameters.AddWithValue("@PONumber", model.SaveUpdateClientInfo.PONumber);
                    cmd.Parameters.AddWithValue("@DatePOExpiry", model.SaveUpdateClientInfo.DatePOExpiry);

                   cmd.ExecuteNonQuery();
                   foreach (var item in model.ReportEmailList)
                        {
                   InserUpdateReportMailList(item);
                        }
                    return true;
                }
            }
           catch (Exception ex)
            {
                throw ex;
                return false;
            }
        }

public void InserUpdateReportMailList(ReportEmailList model)
{
       try{
 using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertUpdateReoprtMailList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                     cmd.Parameters.AddWithValue("@ID", model.id);
                      cmd.Parameters.AddWithValue("@ClientID", model.ClientId);
                    cmd.Parameters.AddWithValue("@EmailAddress", model.emailAddress);
                    if( model.checkEmailAddress==true)
                    {
                    cmd.Parameters.AddWithValue("@checkEmailAddress", 1);  
                    }  
                    else{
                         cmd.Parameters.AddWithValue("@checkEmailAddress", 0);
                    }           
                   cmd.ExecuteNonQuery();
                }
       }
                     catch (Exception ex)
            {
                throw ex;
               
            }

}



        public Boolean UpdateClientProcessInfo(SaveUpdateClientProcessInfoViewModel model)
        {
            try{
 using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateCRMClientProcessInfo", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@Id",model.SaveUpdateClientInfo.ID);
                    cmd.Parameters.AddWithValue("@ClientID", model.SaveUpdateClientInfo.ClientId);
                    cmd.Parameters.AddWithValue("@DateOfInformation", model.SaveUpdateClientInfo.DateOfInformation);
                    cmd.Parameters.AddWithValue("@AgreedSLAId", model.SaveUpdateClientInfo.AgreedSLAId);
                    cmd.Parameters.AddWithValue("@DefinedPersonalisedSLA", model.SaveUpdateClientInfo.DefinedSLA);
                    cmd.Parameters.AddWithValue("@ReportingFormatId", model.SaveUpdateClientInfo.ReportingFormat);
                    cmd.Parameters.AddWithValue("@EmailAddress", model.SaveUpdateClientInfo.EncryptedEmailAddress);
                    cmd.Parameters.AddWithValue("@IsTLSEnabled", model.SaveUpdateClientInfo.IsTLSEnabled);
                    cmd.Parameters.AddWithValue("@TLSDate", model.SaveUpdateClientInfo.TLSDate);
                    cmd.Parameters.AddWithValue("@EncryptedPassword", model.SaveUpdateClientInfo.EncryptedPassword);
                    cmd.Parameters.AddWithValue("@InvoiceFrequencyId ", model.SaveUpdateClientInfo.InvoiceFrequency);
                    cmd.Parameters.AddWithValue("@SpecificInstruction", model.SaveUpdateClientInfo.SpecificInstruction);
                     cmd.Parameters.AddWithValue("@ModifiedBy", model.SaveUpdateClientInfo.LastModifiedBy);
                    cmd.Parameters.AddWithValue("@IsOrederRequired", model.SaveUpdateClientInfo.IsPurchesOrderRequired);
                    cmd.Parameters.AddWithValue("@PONumber", model.SaveUpdateClientInfo.PONumber);
                    cmd.Parameters.AddWithValue("@DatePOExpiry", model.SaveUpdateClientInfo.DatePOExpiry);
                   cmd.ExecuteNonQuery();
                   if(model.SaveUpdateClientInfo.ReportingFormat == 16030)
                   {
                      foreach (var item in model.ReportEmailList)
                        {
                   InserUpdateReportMailList(item);
                        }
                   }else{}
                    return true;
                }
            }
           catch (Exception ex)
            {
                throw ex;
                return false;
            }
        }


        

        public int UpdateCrmClientRecord(UpdateCRMClientRecord model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateCRMClient", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };


                    cmd.Parameters.AddWithValue("@ClientID", model.ClientID);
                    cmd.Parameters.AddWithValue("@ClientTypeId", model.ClientTypeId);
                    cmd.Parameters.AddWithValue("@CompanyName", model.CompanyName);
                    cmd.Parameters.AddWithValue("@HeadOfficeAddress", model.HeadOfficeAddress);
                    cmd.Parameters.AddWithValue("@RegionId", model.RegionId);
                    cmd.Parameters.AddWithValue("@Town", model.Town);
                    cmd.Parameters.AddWithValue("@Postcode", model.Postcode);
                    cmd.Parameters.AddWithValue("@LandlineNo", model.LandlineNo);
                    cmd.Parameters.AddWithValue("@MobileNo", model.MobileNo);
                    cmd.Parameters.AddWithValue("@PrincipalContactName", model.PrincipalContactName);
                    cmd.Parameters.AddWithValue("@PrincipalContactNo", model.PrincipalContactNo);
                    cmd.Parameters.AddWithValue("@PrincipalContactEmail", model.PrincipalContactEmail);
                    cmd.Parameters.AddWithValue("@BrokerNames", model.BrokerId);
                    cmd.Parameters.AddWithValue("@PensionScheme", model.PensionScheme);
                    cmd.Parameters.AddWithValue("@FinalSalary", model.FinalSalary);
                    cmd.Parameters.AddWithValue("@IllHealthEarlyRetirement", model.IllHealthEarlyRetirement);
                    cmd.Parameters.AddWithValue("@IPInsurerName", model.IPInsurerName);
                    cmd.Parameters.AddWithValue("@DeferredPeriod", model.DeferredPeriod);
                    cmd.Parameters.AddWithValue("@Duration", model.Duration);
                    cmd.Parameters.AddWithValue("@RenewalDue", model.RenewalDue);
                    cmd.Parameters.AddWithValue("@DefinitionOfOccupation", model.DefinitionOfOccupation);
                    cmd.Parameters.AddWithValue("@MIInsurerAdminName", model.MIInsurerAdminName);
                    cmd.Parameters.AddWithValue("@CPInsurerName", model.CPInsurerName);
                    cmd.Parameters.AddWithValue("@EAPProviderName", model.EAPProviderName);
                    cmd.Parameters.AddWithValue("@DaysPaidSickLeave", model.DaysPaidSickLeave);
                    cmd.Parameters.AddWithValue("@OHPprovider", model.OHPprovider);
                    cmd.Parameters.AddWithValue("@Recording", model.Recording);
                    cmd.Parameters.AddWithValue("@RelationshipOwner", model.RelationshipOwner);
                    cmd.Parameters.AddWithValue("@MeetingIntervals", model.MeetingIntervals);
                    cmd.Parameters.AddWithValue("@FormalContractReqd", model.FormalContractReqd);
                    cmd.Parameters.AddWithValue("@ContractSignedDate", string.IsNullOrEmpty(model.ContractSignedDate) ? null : model.ContractSignedDate);
                    cmd.Parameters.AddWithValue("@ContractExpiryDate", string.IsNullOrEmpty(model.ContractExpiryDate) ? null : model.ContractExpiryDate);
                    cmd.Parameters.AddWithValue("@ConsentToUseLogo", model.ConsentToUseLogo);
                    cmd.Parameters.AddWithValue("@IsActive", model.Status);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", model.LastModifiedBy);
                    cmd.Parameters.AddWithValue("@ClientServices", model.ClientServices);
                    cmd.Parameters.AddWithValue("@RefSourceId", model.RefSourceId);

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataSet DeleteClientRecord(int ClientID)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteClientRecord", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientID", ClientID);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;

        }


        
        public DataSet DeleteEmailList(int ID)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteEmailAddress", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ID", ID);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;

        }

        public DataTable GetReferralContactGrid(int ClientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientReferralContacts", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetReferralContactById(int ClientReferralContactsId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientReferralContact ", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientReferralContactsId", ClientReferralContactsId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int SaveReferralContact(SaveReferralContact model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertClientReferralContacts", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientId", model.ClientId);
                    cmd.Parameters.AddWithValue("@ContactName", model.ContactName);
                    cmd.Parameters.AddWithValue("@JobTitle", model.JobTitle);
                    cmd.Parameters.AddWithValue("@PhoneNo", model.PhoneNo);
                    cmd.Parameters.AddWithValue("@NonHOLocation", model.NonHOLocation);
                    cmd.Parameters.AddWithValue("@ContactEmail", model.ContactEmail);
                    cmd.Parameters.AddWithValue("@CreatedBy", model.CreatedBy);
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateReferralContact(UpdateReferralContact model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateClientReferralContacts", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientReferralContactsId", model.ClientReferralContactsId);
                    cmd.Parameters.AddWithValue("@ContactName", model.ContactName);
                    cmd.Parameters.AddWithValue("@JobTitle", model.JobTitle);
                    cmd.Parameters.AddWithValue("@PhoneNo", model.PhoneNo);
                    cmd.Parameters.AddWithValue("@NonHOLocation", model.NonHOLocation);
                    cmd.Parameters.AddWithValue("@ContactEmail", model.ContactEmail);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", model.LastModifiedBy);

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet DeleteReferralcontact(int ClientReferralContactsId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteReferalContact ", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientReferralcontactsId", ClientReferralContactsId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;

        }


        public DataTable GetAccountContactGrid(int ClientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientAccountContacts ", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetAccountContactById(int ClientAccountContactsId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientAccountContact", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", ClientAccountContactsId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public int SaveAccountContact(SaveAccountContact model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    var Password = "Password@123";
                    var ClientContactPwd = Encryption.Encrypt(Password);
                    SqlCommand cmd = new SqlCommand("InsertClientAccountContacts", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@UserId";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);

                    cmd.Parameters.AddWithValue("@ClientId", model.clientId);
                    cmd.Parameters.AddWithValue("@ActContactName", model.fullName);
                    cmd.Parameters.AddWithValue("@ActContactEmail", model.emailAddress);
                    cmd.Parameters.AddWithValue("@ActPhoneNo", model.telephoneNumber);
                    cmd.Parameters.AddWithValue("@Instructions", model.instructions);
                    cmd.Parameters.AddWithValue("@PaymentTerms", model.PaymentTerms);
                    cmd.Parameters.AddWithValue("@ClientContactPwd", ClientContactPwd);
                    cmd.Parameters.AddWithValue("@CreatedBy", model.CreatedBy);
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateAccountContact(UpdateAccountContact model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateClientAccountContacts", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", model.ClientAccountContactsId);
                    cmd.Parameters.AddWithValue("@ActContactName", model.fullName);
                    cmd.Parameters.AddWithValue("@ActContactEmail", model.emailAddress);
                    cmd.Parameters.AddWithValue("@ActPhoneNo", model.telephoneNumber);
                    cmd.Parameters.AddWithValue("@Instructions", model.Instructions);
                    cmd.Parameters.AddWithValue("@PaymentTerms", model.PaymentTerms);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", model.LastModifiedBy);

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet DeleteAccountcontact(int UserId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteAccountcontact ", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;

        }
        public DataSet GetNotifyEmailId(int userId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetNotifyEmailList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", userId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return (ds);
        }


        public Boolean SendNoteEmail(string CompanyName, int userId, string UserName, int Noteid)
        {
            try
            {
                DataSet ds;
                StringBuilder sbMailBody = new StringBuilder();
                ds = GetNotifyEmailId(userId);
                List<string> MailList = new List<string>();
                string MailSubject = "Changes made to " + CompanyName;
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    MailList.Add(row["EmailAddress"].ToString());

                }
                object[] arry = MailList.ToArray();

             sbMailBody.Append("There have been some notes added to the client record for " + CompanyName + " that are considered as important. Please log in to ORCA for an update"
               + "<br/></br><span></span><p>HCB Group ORCA</p>");

                bool result = SendEmail(arry, sbMailBody, MailSubject);
                if (result == true)
                {

                    try
                    {
                        using (SqlConnection conn = new SqlConnection(connectionstring))
                        {
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("UpdateClientNoteForNotify", conn)
                            {
                                CommandType = CommandType.StoredProcedure
                            };

                            cmd.Parameters.AddWithValue("@NoteId", Noteid);
                            cmd.ExecuteNonQuery();
                            // dashboardAdapter.Fill(ds);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                // UpdateClientNote()
            }

            catch (Exception ex)
            {

                return false;
            }
            return true;
        }
        private static bool SendEmail(object[] arry, StringBuilder sbMailBody, string MailSubject)
        {
            try
            {
                MailMessage mail = new MailMessage();
                if (arry.Length > 0)
                {
                    foreach (string stringlist in arry)
                    {
                        if (!string.IsNullOrEmpty(stringlist))
                        {
                            mail.To.Add(stringlist);
                        }
                    }
                }

              //  mail.Bcc.Add(System.Configuration.ConfigurationManager.AppSettings["BCCEmailAddress"].ToString());
                mail.Subject = MailSubject;
                mail.Body = sbMailBody.ToString();
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SmtpHost"].ToString());
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
            
              //  System.Net.NetworkCredential objAuthentication = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SmtpUserName"].ToString(), System.Configuration.ConfigurationManager.AppSettings["SmtpPassword"].ToString());

                smtp.UseDefaultCredentials =true;
                smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SmtpPort"]);
                smtp.EnableSsl = false;

              //  smtp.Credentials = objAuthentication;
                mail.ReplyTo = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
                smtp.Send(mail);
                sbMailBody.Clear();
                return true;
            }
            // catch (SmtpException ex)
            // {
            //     throw ex.InnerException;

            // }
            catch (Exception ex)
            {
            
                //throw ex;
                return false;
            }

        }



        public Boolean SendNewClientAddedEmail(string UserName, int userId, string CompanyName)
        {
            try
            {
                DataSet ds;
                StringBuilder sbMailBody = new StringBuilder();
                ds = GetNotifyEmailId(userId);
                List<string> MailList = new List<string>();
                //ds = GetNotifyEmailId(userId);
                string MailSubject = "New Client Added By " + UserName;

                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    MailList.Add(row["EmailAddress"].ToString());

                }
                object[] arry = MailList.ToArray();
                sbMailBody.Append("A new client – " + CompanyName + "  has been added. " + "<br/></br><span></span><p>HCB Group ORCA</p>");
                SendEmail(arry, sbMailBody, MailSubject);
            }
            catch (Exception ex)
            {

                return false;
            }
            return true;
        }


        public DataTable GetNote(int ClientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientNotes", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int SaveNote(SaveNoteModel model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertClientNotes", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@NoteId";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);

                    cmd.Parameters.AddWithValue("@ClientId", model.ClientId);
                    cmd.Parameters.AddWithValue("@NoteCreatedByUserId", model.NoteCreatedByUserId);
                    cmd.Parameters.AddWithValue("@Description", model.description);
                    cmd.Parameters.AddWithValue("@NextActionDate", model.NextActionDate);
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    Noteid = result;
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetClientServices(int ClientTypeId, int ClientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientServicetypes", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientTypeId", ClientTypeId);
                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int SaveDocument(UploadFile model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertClientDocuments", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientID", model.ClientId);
                    cmd.Parameters.AddWithValue("@DocumentName", model.DocumentName);
                    cmd.Parameters.AddWithValue("@DocumentPath", model.DocumentPath);
                    cmd.Parameters.AddWithValue("@UploadedBy", model.UploadedBy);

                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetClientDocuments(int ClientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientDocuments", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }


        public DataTable GetRelationshipOwner()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetRelationshipOwner", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }




        public DataTable GetBroker()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetBroker", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable SaveRelationShipOwner(SaveRelationshipOwner model)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertRelationshipOwner", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@OwnerId";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);

                    cmd.Parameters.AddWithValue("@OwnerName", model.OwnerName);
                    cmd.Parameters.AddWithValue("@EmailAddress", model.EmailAddress);
                    cmd.Parameters.AddWithValue("@CreatedBy", model.CreatedBy);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable SaveBroker(SaveBroker model)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertBroker", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@BrokerId";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);

                    cmd.Parameters.AddWithValue("@BrokerName", model.BrokerName);
                    cmd.Parameters.AddWithValue("@EmailAddress", model.EmailAddress);
                    cmd.Parameters.AddWithValue("@CreatedBy", model.CreatedBy);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataTable GetOwnerById(int OwnerId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetRelationshipOwnerById", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@OwnerId", OwnerId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetBrokerById(int BrokerId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetBrokerById", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@BrokerId", BrokerId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int UpdateRelationShipOwner(SaveRelationshipOwner model)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateRelationshipOwner", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@OwnerId", model.OwnerId);
                    cmd.Parameters.AddWithValue("@OwnerName", model.OwnerName);
                    cmd.Parameters.AddWithValue("@EmailAddress", model.EmailAddress);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", model.LastModifiedBy);

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int UpdateBroker(SaveBroker model)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateBroker", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@BrokerId", model.BrokerId);
                    cmd.Parameters.AddWithValue("@BrokerName", model.BrokerName);
                    cmd.Parameters.AddWithValue("@EmailAddress", model.EmailAddress);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", model.LastModifiedBy);

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataSet CheckCompanyName(string CompanyName, int ClientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("checkCompanyName", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@CompanyName", CompanyName);
                    cmd.Parameters.AddWithValue("@ClientID", ClientId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            ds.Tables[0].TableName = "CompanyName";
            return (ds);
        }

        public DataSet CheckRealtionShipOwner(string Name)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CheckRelationShipOwner", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Name", Name);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            ds.Tables[0].TableName = "Name";
            return (ds);
        }

        public DataSet DeleteDocument(int Id, int LastModifiedBy)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteDocumnet", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", LastModifiedBy);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        public string DownloadDocument(int Id)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientDocumentsById", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@DocumentId", Id);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0].Rows[0]["DocumentPath"].ToString();
        }

        public DataTable SearchNoteFromList(string NoteInput, int clientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SearchNote", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@NoteInput", NoteInput);
                    cmd.Parameters.AddWithValue("@clientId", clientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }


    }
}

