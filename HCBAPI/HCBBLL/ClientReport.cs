using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HCBNewAPI.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Net.Mail;
using System.Configuration;
using System.Globalization;



namespace HCBNewAPI.HCBBLL
{
    public class ClientReport
    {
        IConfiguration _configuration;
        public string connectionstring;
        private Database db;


        public ClientReport(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionstring = _configuration["ConnectionStrings:DefaultConnection"]; //DefualtConnectionstring
        }

        public DataSet GetClientReportList(GetClientReport model)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Get_HCBClientsReport", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    
                     cmd.Parameters.AddWithValue("@pageIndex", model.pageIndex);
                     cmd.Parameters.AddWithValue("@pageSize", model.pageSize);                                    
                     cmd.Parameters.AddWithValue("@sort", model.sort);                               
                     cmd.Parameters.AddWithValue("@direction", model.direction);
                     cmd.Parameters.AddWithValue("@CompanyName", model.CompanyName);
                      cmd.Parameters.AddWithValue("@export", model.export); 
                     if(!string.IsNullOrEmpty( model.ClientTypeId))
                    {
                     cmd.Parameters.AddWithValue("@ClientTypeId", model.ClientTypeId);
                    }
                    else
                    {
                     cmd.Parameters.AddWithValue("@ClientTypeId", null);
                    }
                     cmd.Parameters.AddWithValue("@ClientCreatedDateFrom", model.ClientCreatedDateFrom =="" ? null:model.ClientCreatedDateFrom); 
                     cmd.Parameters.AddWithValue("@ClientCreatedDateTo", model.ClientCreatedDateTo =="" ? null:model.ClientCreatedDateTo); 
                   cmd.Parameters.AddWithValue("@PrincipalContactEmail", model.PrincipalContactEmail);
                     if(!string.IsNullOrEmpty( model.ClientServices)){
                     cmd.Parameters.AddWithValue("@ClientServices", model.ClientServices);
                   }
                  else
                   {
                    cmd.Parameters.AddWithValue("@ClientServices", null);
                   }
                    cmd.Parameters.AddWithValue("@RelationShipOwnerId", model.RelationShipOwnerId);
                    cmd.Parameters.AddWithValue("@RegionId", model.RegionId);
                   
                    if(model.IsActive == 0 || model.IsActive ==1 )
                    {
                    cmd.Parameters.AddWithValue("@IsActive", model.IsActive);
                    }
                    else
                    {
                         cmd.Parameters.AddWithValue("@IsActive", null);
                    }
                   SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
              ds.Tables[0].TableName = "GetClientReport";
              ds.Tables[1].TableName = "HCBClientLength";
              return (ds);
        }
          public DataTable GetddlServiceList()
          {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetddlServiceTypesList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }


    }
}
